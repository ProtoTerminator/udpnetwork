﻿//------------------------------------------
// Author: Tim Cassell
// Created: May 09, 2016
//------------------------------------------

using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System;
using System.Threading;
using System.Collections.Generic;
using SerializeCompact;
using System.Linq;

namespace UDPNetwork
{
    public class NetworkManager : MonoBehaviour
    {
        struct DataPacket
        {
            public IPEndPoint destination;
            public byte[] data;

            public DataPacket(IPEndPoint destination, byte[] data)
            {
                this.destination = destination;
                this.data = data;
            }
        }

        class LivingObject
        {
            public GameObject go;
            public NetworkIdentity ni;
            public bool DestroyMe;
        }

        [SerializeField]
        string SERVER_IP = "71.163.19.142";
        [SerializeField]
        ushort SERVER_PORT = 55566;
        [SerializeField]
        int MAX_PLAYERS_PER_SERVER = 2000;
        const uint PROTOCOL_ID = 3764243738u;

        public GameObject playerObject;
        public List<GameObject> spawnPrefabs;
        public int timeoutMs = 5000;
        public int simulatedPing = 0;

        Dictionary<IPEndPoint, ConnectedPlayer> connectedPlayers = new Dictionary<IPEndPoint, ConnectedPlayer>();
        HashSet<NetworkInstanceID> connectedIDs = new HashSet<NetworkInstanceID>();
        void AddConnectedPlayer(ConnectedPlayer player)
        {
            connectedPlayers.Add(player.playerEndPoint, player);
            connectedIDs.Add(player.playerID);
            queuedSendData.Add(player.playerEndPoint, new Queue<SerializeCompact.BitStream>());
        }
        void RemoveConnectedPlayer(ConnectedPlayer player)
        {
            connectedIDs.Remove(player.playerID);
            connectedPlayers.Remove(player.playerEndPoint);
            queuedSendData.Remove(player.playerEndPoint);
        }

#if SERVER
        uint playerIDs = 0;
        uint FreshPlayerID
        {
            get
            {
                while (connectedIDs.Contains(new NetworkInstanceID(playerIDs)) || playerIDs == 0)
                {
                    playerIDs++;
                }
                return playerIDs;
            }
        }
        uint objectIDs = 0;
        uint FreshObjectID
        {
            get
            {
                while (livingObjects.ContainsKey(new NetworkInstanceID(objectIDs)) || objectIDs == 0)
                {
                    objectIDs++;
                }
                return objectIDs;
            }
        }
#endif
        Dictionary<NetworkInstanceID, LivingObject> livingObjects = new Dictionary<NetworkInstanceID, LivingObject>();
        Queue<SpawnInfo> InstantiateObjects = new Queue<SpawnInfo>();
        AsyncPriorityQueue<DataPacket> queuedReceiveData;
        Dictionary<IPEndPoint, Queue<SerializeCompact.BitStream>> queuedSendData = new Dictionary<IPEndPoint, Queue<SerializeCompact.BitStream>>();

        Socket sck;
        IPEndPoint ipEndPoint;
        bool listening = true;
        bool processing = true;

        public static NetworkManager instance
        {
            get;
            private set;
        }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this);
            }
            else
                Destroy(this);
        }

        void Start()
        {
            queuedReceiveData = new AsyncPriorityQueue<DataPacket>(MAX_PLAYERS_PER_SERVER, false);
            sck = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
#if SERVER
			ipEndPoint = new IPEndPoint(IPAddress.Any, SERVER_PORT);
			sck.Bind(ipEndPoint);
#else
            ipEndPoint = new IPEndPoint(IPAddress.Parse(SERVER_IP), SERVER_PORT);
            sck.Bind(new IPEndPoint(IPAddress.Any, 0));
#endif
            new Thread(() => ListenForData()).Start();
            new Thread(() => ProcessData()).Start();
            StartCoroutine(SendFinally());
            
#if !SERVER
            AddConnectedPlayer(new ConnectedPlayer(ipEndPoint, new NetworkInstanceID()));

            SerializeCompact.BitStream bs = new SerializeCompact.BitStream();
            bs.Write(Headers.Connect, Packet.minHeader, Packet.maxHeader);
            SendData(bs);
#endif
            StartCoroutine(SendPing());
        }

        IEnumerator SendPing()
        {
            SerializeCompact.BitStream bs = new SerializeCompact.BitStream();
            bs.Write(Headers.Ping, Packet.minHeader, Packet.maxHeader);

            while (true)
            {
                yield return new WaitForSeconds(0.1f);
                
                foreach (var player in connectedPlayers)
                {
                    if (player.Value.RoundTripTime > 250 || player.Value.TimeSinceLastPacketSent > 500)
                    {
                        //player.Value.countPing = true;
                        SendDataTo(bs, player.Key);
                    }
                }
            }
        }

        void OnGUI()
        {
            int counter = 0;
            foreach (var player in connectedPlayers)
            {
                GUI.Box(new Rect(0, counter, Screen.width / 4, Screen.height / 16), "Player " + player.Value.playerID.Value + " ping: " + player.Value.RoundTripTime);
                counter += Screen.height / 16 + 1;
            }
        }

        void Update()
        {
            lock (connectedPlayers)
            {
                //Debug.LogError(connectedPlayers.Count);
                List<IPEndPoint> removeKeys = new List<IPEndPoint>();
                foreach (var player in connectedPlayers)
                {
                    /*if (player.Value.countPing)
                    {
                        player.Value.AddPingTime(Time.deltaTime * 1000f);
                    }*/
#if SERVER
                    if (player.Value.RoundTripTime > timeoutMs) // if it's been longer than timeout since a message was received
                    {
                        Debug.LogError("Ping time-out!");
                        foreach (var id in player.Value.netIDs)
                        {
                            livingObjects[id].DestroyMe = true;
                        }
                        removeKeys.Add(player.Key);
                    }
#endif
                }
                foreach (var key in removeKeys)
                {
                    //connectedPlayers.Remove(key);
                    RemoveConnectedPlayer(connectedPlayers[key]);
                }
            }
            lock (InstantiateObjects)
            {
                while (InstantiateObjects.Count > 0)
                {
                    var info = InstantiateObjects.Dequeue();
                    GameObject go = Instantiate(info.obj);
                    go.GetComponent<NetworkIdentity>().SetNetID(info.netID);
                    go.GetComponent<NetworkIdentity>().SetLocalPlayer(info.localPlayer);
                    lock (livingObjects)
                    {
                        livingObjects.Add(info.netID, new LivingObject() { go = info.obj, ni = go.GetComponent<NetworkIdentity>() });
                    }
                }
            }
            lock (livingObjects)
            {
                List<NetworkInstanceID> removeKeys = new List<NetworkInstanceID>();
                foreach (var obj in livingObjects)
                {
                    if (obj.Value.DestroyMe)
                    {
                        Debug.LogWarning("Destroying object " + obj.Key.Value + "...");
#if SERVER
                        SerializeCompact.BitStream bs = new SerializeCompact.BitStream();
                        bs.Write(Headers.Destroy, Packet.minHeader, Packet.maxHeader);
                        bs.Write(obj.Value.ni.netID.Value, uint.MinValue, uint.MaxValue);
                        SendData(bs);
#endif
                        removeKeys.Add(obj.Key);
                    }
                }
                foreach(var key in removeKeys)
                {
                    Destroy(livingObjects[key].ni.gameObject);
                    livingObjects.Remove(key);
                }
            }
        }

        public void Disconnect()
        {
            foreach (var obj in livingObjects)
            {
                if (obj.Value.ni.IsLocalPlayer)
                {
                    SerializeCompact.BitStream bs = new SerializeCompact.BitStream();
                    bs.Write(Headers.Destroy, Packet.minHeader, Packet.maxHeader);
                    bs.Write(obj.Value.ni.netID.Value, uint.MinValue, uint.MaxValue);
                    SendData(bs);
                }
            }
        }

        void OnDestroy()
        {
            Disconnect();

            listening = false;
            processing = false;
            sck.Close();
        }

        public void SendData(SerializeCompact.BitStream data, sbyte importance = 0)
        {
            SendDataTo(data, ipEndPoint, importance);
        }

        public void SendDataTo(SerializeCompact.BitStream data, IPEndPoint endPoint, sbyte importance = 0)
        {
            data = data.Clone();
            if (!queuedSendData.ContainsKey(endPoint))
            {
                Debug.LogError("queuedSendData doesn't contain the given IPEndPoint");
                //queuedSendData.Add(endPoint, new Queue<SerializeCompact.BitStream>());
                return;
            }
            ConnectedPlayer player = connectedPlayers[endPoint];
            data.Write(PROTOCOL_ID, uint.MinValue, uint.MaxValue);
            WriteHeaders(data, importance, player.LocalPacketID, player.RemotePacketID, player.RemotePacketBitField);
            queuedSendData[endPoint].Enqueue(data);
            player.LocalPacketID++;
            SetupPacketProtocol(queuedSendData[endPoint], player);
        }

        IEnumerator SendFinally()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();

                foreach (var player in queuedSendData)
                {
                    while (player.Value.Count > 0)
                    {
                        var packet = player.Value.Dequeue();
                        InputCrc(packet);
                        StartCoroutine(SendDelayed(player.Key, packet.GetByteArray()));
                        connectedPlayers[player.Key].ResetPacketTimer();
                        connectedPlayers[player.Key].StartRTTMeasure();
                        connectedPlayers[player.Key].MostRecentPingID = connectedPlayers[player.Key].LocalPacketID;
                    }
                }
            }
        }

        IEnumerator SendDelayed(IPEndPoint dest, byte[] data)
        {
            yield return new WaitForSecondsRealtime(simulatedPing / 2000f);
            sck.SendTo(data, 0, data.Length, SocketFlags.None, dest);
            Debug.LogWarning("sent data");
            //Debug.LogError("sent message containing " + packet.StoredBits + " bits");
        }

        void SetupPacketProtocol(Queue<SerializeCompact.BitStream> packets, ConnectedPlayer player)
        {
            if (packets.Count < 2)
            {
                if (packets.Count == 1)
                {
                    var data = packets.Dequeue();
                    var frags = FragmentPacket(data, 512 - 7);
                    foreach (var frag in frags)
                    {
                        packets.Enqueue(frag);
                    }
                }
                return;
            }
            for (int counter = packets.Count; counter > 2; counter--)
            {
                packets.Enqueue(packets.Dequeue());
            }
            var data1 = packets.Dequeue();
            var data2 = packets.Dequeue();

            sbyte importance;
            byte fragCount;
            //uint sequence;
            data1.Read(out importance, sbyte.MinValue, sbyte.MaxValue);
            data1.Read(out fragCount, byte.MinValue, byte.MaxValue);
            //data1.Read(out sequence, uint.MinValue, uint.MaxValue);
            var tempBS = data1.Clone();
            data1.Clear();
            data1.Write(importance, sbyte.MinValue, sbyte.MaxValue);
            data1.Write(fragCount, byte.MinValue, byte.MaxValue);
            //data1.Write(sequence, uint.MinValue, uint.MaxValue);
            data1.Merge(tempBS);

            var frags2 = FragmentPacket(data2, 512 - 7);

            if (fragCount > 1 || frags2.Length > 1 || Utilities.IntDivideRoundUp(data1.StoredBits, 8) + Utilities.IntDivideRoundUp(data2.StoredBits, 8) > 508)
            {
                packets.Enqueue(data1);
                foreach (var frag in frags2)
                {
                    packets.Enqueue(frag);
                }
            }
            else
            {
                CombinePackets(data1, frags2[0]);
                packets.Enqueue(data1);
                player.LocalPacketID--;
            }
        }

        // add fragcount (1 byte) and frag id/packet count (1 byte)
        SerializeCompact.BitStream[] FragmentPacket(SerializeCompact.BitStream packet, uint maxBytesPerPacket)
        {
            long bytes = Utilities.IntDivideRoundUp(packet.StoredBits, 8);
            /*if (bytes < maxBytesPerPacket)
            {
                return new SerializeCompact.BitStream[] { packet };
            }*/
            sbyte importance;
            uint sequence;
            packet.Read(out importance, sbyte.MinValue, sbyte.MaxValue);
            packet.Read(out sequence, uint.MinValue, uint.MaxValue);
            long fragCount = Utilities.IntDivideRoundUp(bytes, maxBytesPerPacket);
            SerializeCompact.BitStream[] packets = new SerializeCompact.BitStream[fragCount];
            SerializeCompact.BitStream data;
            byte i = 0;
            for (; i < fragCount - 1; i++)
            {
                data = new SerializeCompact.BitStream(maxBytesPerPacket * 8L);
                data.Write(importance, sbyte.MinValue, sbyte.MaxValue);
                data.Write(fragCount, byte.MinValue, byte.MaxValue);
                data.Write(i, byte.MinValue, byte.MaxValue); // fragment id
                data.Write(sequence, uint.MinValue, uint.MaxValue);
                data.Merge(packet, maxBytesPerPacket * 8L);
                packets[i] = data;
            }
            data = new SerializeCompact.BitStream(packet.StoredBits + 16);
            data.Write(importance, sbyte.MinValue, sbyte.MaxValue);
            data.Write(fragCount, byte.MinValue, byte.MaxValue);
            data.Write((fragCount > 1 ) ? i : 1L, byte.MinValue, byte.MaxValue); // fragment id or packet count
            data.Write(sequence, uint.MinValue, uint.MaxValue);
            data.Merge(packet);
            packets[i] = data;
            packet.Clear();
            return packets;
        }

        // merge importance, fragcount, fragid/packetcount, sequence
        // separate ack, ackbitfield, and data
        void CombinePackets(SerializeCompact.BitStream packet1, SerializeCompact.BitStream packet2)
        {
            sbyte importance;
            sbyte importance2;
            byte fragCount;
            byte packetCount;
            uint sequence;
            packet2.Read(out importance2, sbyte.MinValue, sbyte.MaxValue);
            packet2.Read(out fragCount, byte.MinValue, byte.MaxValue);
            packet2.Read(out packetCount, byte.MinValue, byte.MaxValue);
            packet2.Read(out sequence, uint.MinValue, uint.MaxValue);

            packet1.Read(out importance, sbyte.MinValue, sbyte.MaxValue);
            packet1.Read(out fragCount, byte.MinValue, byte.MaxValue);
            packet1.Read(out packetCount, byte.MinValue, byte.MaxValue);
            packet1.Read(out sequence, uint.MinValue, uint.MaxValue);

            packetCount++;
            var tempBS = packet1.Clone();
            packet1.Clear();
            packet1.Write((importance < importance2) ? importance : importance2, sbyte.MinValue, sbyte.MaxValue);
            packet1.Write(fragCount, byte.MinValue, byte.MaxValue);
            packet1.Write(packetCount, byte.MinValue, byte.MaxValue);
            packet1.Write(sequence, uint.MinValue, uint.MaxValue);
            packet1.Merge(tempBS);
            packet1.Merge(packet2);
        }

        // add importance (1 byte), sequence (4 bytes), ack (4 bytes), and ackbitfield (4 bytes)
        void WriteHeaders(SerializeCompact.BitStream packet, sbyte importance, uint sequence, uint ack, uint ackBitField)
        {
            var newBS = packet.Clone();
            packet.Clear();
            packet.Write(importance, sbyte.MinValue, sbyte.MaxValue);
            packet.Write(sequence, uint.MinValue, uint.MaxValue);
            packet.Write(ack, uint.MinValue, uint.MaxValue);
            packet.Write(ackBitField, uint.MinValue, uint.MaxValue);
            packet.Merge(newBS);
        }

        // add crc32 (4 bytes)
        void InputCrc(SerializeCompact.BitStream packet)
        {
            uint crc = CRC32.GenerateCrc32(packet.GetByteArray()) ^ PROTOCOL_ID;
            SerializeCompact.BitStream temp = packet.Clone();
            packet.Clear();
            packet.Write(crc, uint.MinValue, uint.MaxValue);
            packet.Merge(temp);
        }

        void ListenForData()
        {
            while (listening)
            {
                EndPoint endPoint = ipEndPoint;

                byte[] buffer = new byte[512];
                int rec = sck.ReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref endPoint);
                Array.Resize(ref buffer, rec);

                queuedReceiveData.Enqueue(new DataPacket((IPEndPoint) endPoint, buffer), 0);
                /*if (connectedPlayers.ContainsKey((IPEndPoint) endPoint))
                {
                    Debug.LogError("received message from Player " + connectedPlayers[(IPEndPoint) endPoint].playerID.Value);
                }
                else
                {
                    Debug.LogError("received message from unknown");
                }*/
            }
        }

        void ProcessData()
        {
            DataPacket rcv;
            byte[] data;
            IPEndPoint ep;
            SerializeCompact.BitStream packet;
            uint crc = 0;
            uint crcRead;
            sbyte importance;
            byte fragCount;
            byte fragIdPacketCount;
            uint sequence;
            uint ack;
            uint ackBitfield;
            Headers header;
            uint protocol;
            long bits;

            while (processing)
            {
                rcv = queuedReceiveData.Dequeue();
                Debug.LogError("received message");
                data = rcv.data;
                ep = rcv.destination;

                packet = new SerializeCompact.BitStream(data);
                bits = packet.StoredBits;
                
                packet.Read(out crc, uint.MinValue, uint.MaxValue);
                crcRead = CRC32.GenerateCrc32(packet.GetByteArray()) ^ PROTOCOL_ID;
                
                if (crc != crcRead)
                {
                    Debug.LogError("CRC mismatch! Aborting packet read." + "\nbits: " + bits);
                    continue;
                }
                packet.Read(out importance, sbyte.MinValue, sbyte.MaxValue);
                packet.Read(out fragCount, byte.MinValue, byte.MaxValue);
                packet.Read(out fragIdPacketCount, byte.MinValue, byte.MaxValue);
                packet.Read(out sequence, uint.MinValue, uint.MaxValue);

                ConnectedPlayer player = null;

                if (connectedPlayers.ContainsKey(ep))
                {
                    player = connectedPlayers[ep];
                    if (player.RemotePacketFound(sequence)) // skip any duplicate packets
                    {
                        Debug.LogError("skipping duplicate");
                        continue;
                    }
                    player.RemotePacketID = sequence;
                    if (fragCount > 1)
                    {
                        player.fragmentedPackets.Add(sequence, fragCount, fragIdPacketCount, packet);
                        packet = player.fragmentedPackets.GetNextComplete(sequence);
                        player.fragmentedPackets.RemoveOld(sequence, 64);
                        if (packet == null)
                        {
                            continue;
                        }
                        fragIdPacketCount = 1;
                    }
                }

                for (; fragIdPacketCount > 0; fragIdPacketCount--)
                {
                    // TODO: add new dictionary for important packets, check against ack and ackbitfield
                    packet.Read(out ack, uint.MinValue, uint.MaxValue);
                    packet.Read(out ackBitfield, uint.MinValue, uint.MaxValue);

                    if (!packet.Read(out header, Packet.minHeader, Packet.maxHeader))
                    {
                        Debug.LogError("there was an issue reading the header" + "\nbits: " + bits);
                        break;
                    }
                    Debug.LogError("header: " + header + ", packetCount: " + fragIdPacketCount);

                    switch (header)
                    {
                        case Headers.Connect:
                            {
#if SERVER
                                packet.Read(out protocol, uint.MinValue, uint.MaxValue);
                                if (protocol != PROTOCOL_ID)
                                {
                                    Debug.LogError("There is an issue with the packet. Aborting read." + "\nbits: " + bits);
                                    break;
                                }

                                lock (connectedPlayers)
                                {
                                    uint playerID;
                                    if (player == null)
                                    {
                                        //Debug.LogError("adding player");
                                        playerID = FreshPlayerID;
                                        AddConnectedPlayer(new ConnectedPlayer(ep, new NetworkInstanceID(playerID)));
                                        //connectedPlayers.Add(ep, newPlayer);
                                    }
                                    /*playerID = player.playerID.Value;                             
                                    packet.Clear();
                                    packet.Write(Headers.Initialize, Packet.minHeader, Packet.maxHeader);
                                    packet.Write(playerID, uint.MinValue, uint.MaxValue);
                                    SendDataTo(packet, ep);*/
                                        
                                    lock (livingObjects)
                                    {
                                        foreach (var obj in livingObjects.Values)
                                        {
                                            var newPacket = new SerializeCompact.BitStream();
                                            newPacket.Write(Headers.Spawn, Packet.minHeader, Packet.maxHeader);
                                            newPacket.Write(spawnPrefabs.IndexOf(obj.go), 0, spawnPrefabs.Count);
                                            newPacket.Write(obj.ni.netID.Value, uint.MinValue, uint.MaxValue);
                                            newPacket.Write(obj.ni.IsLocalPlayer);
                                            SendDataTo(newPacket, ep);
                                        }
                                    }
                                    
                                    NetworkInstantiate(playerObject, new NetworkInstanceID(FreshObjectID), ep);
                                }
#else
                                Debug.LogError("Someone is attempting to connect directly!");
#endif
                                break;
                            }
                        case Headers.Initialize:
                            {
                                lock (connectedPlayers)
                                {
                                    uint ID;
                                    packet.Read(out ID, uint.MinValue, uint.MaxValue);
                                    
                                    packet.Read(out protocol, uint.MinValue, uint.MaxValue);
                                    if (protocol != PROTOCOL_ID)
                                    {
                                        Debug.LogError("There is an issue with the packet. Aborting read." + "\nbits: " + bits);
                                        break;
                                    }

                                    AddConnectedPlayer(new ConnectedPlayer(ep, new NetworkInstanceID(ID)));
                                }
                                break;
                            }
                        case Headers.Spawn:
                            {
                                int index;
                                uint netID;
                                bool localPlayer;
                                if (!packet.Read(out index, 0, spawnPrefabs.Count - 1) ||
                                    !packet.Read(out netID, uint.MinValue, uint.MaxValue) ||
                                    !packet.Read(out localPlayer))
                                {
                                    Debug.LogError("there was an issue reading the packet" + "\nbits: " + bits);
                                    break;
                                }
                                
                                packet.Read(out protocol, uint.MinValue, uint.MaxValue);
                                if (protocol != PROTOCOL_ID)
                                {
                                    Debug.LogError("There is an issue with the packet. Aborting read." + "\nbits: " + bits);
                                    break;
                                }
#if SERVER
                                NetworkInstantiate(playerObject, new NetworkInstanceID(playerIDs), ep);
#else
                                NetworkInstantiate(spawnPrefabs[index], new NetworkInstanceID(netID), localPlayer);
#endif
                                Debug.LogError("instantiating object");
                                break;
                            }
                        case Headers.Destroy:
                            {
                                uint netID;
                                if (!packet.Read(out netID, uint.MinValue, uint.MaxValue))
                                {
                                    Debug.LogError("there was an issue reading the packet" + "\nbits: " + bits);
                                    break;
                                }
                                
                                packet.Read(out protocol, uint.MinValue, uint.MaxValue);
                                if (protocol != PROTOCOL_ID)
                                {
                                    Debug.LogError("There is an issue with the packet. Aborting read." + "\nbits: " + bits);
                                    break;
                                }

                                lock (livingObjects)
                                {
                                    NetworkInstanceID id = new NetworkInstanceID(netID);
                                    if (livingObjects.ContainsKey(id))
                                    {
                                        livingObjects[id].DestroyMe = true;
                                    }
                                }
                                break;
                            }
                        case Headers.Ping:
                            {
                                packet.Read(out protocol, uint.MinValue, uint.MaxValue);
                                if (protocol != PROTOCOL_ID)
                                {
                                    Debug.LogError("There is an issue with the packet. Aborting read." + "\nbits: " + bits);
                                    break;
                                }

                                var newPacket = new SerializeCompact.BitStream();
                                newPacket.Write(Headers.Response, Packet.minHeader, Packet.maxHeader);
                                SendDataTo(newPacket, ep);
                                break;
                            }
                        case Headers.Response:
                            {
                                packet.Read(out protocol, uint.MinValue, uint.MaxValue);
                                if (protocol != PROTOCOL_ID)
                                {
                                    Debug.LogError("There is an issue with the packet. Aborting read." + "\nbits: " + bits);
                                    break;
                                }
                                /*
                                SerializeCompact.BitStream bs = new SerializeCompact.BitStream();
                                bs.Write(Headers.Ping, Packet.minHeader, Packet.maxHeader);
                                SendDataTo(bs, ep);*/
                                break;
                            }
                        default:
                            {
                                Debug.LogError("Invalid header. Aborting read." + "\nbits: " + bits);
                                break;
                            }
                    }

                    if (player != null)
                    {
                        player.StopRTTMeasure(ack);
                        //player.ResetPingTime();
                        //player.countPing = false;
                        Debug.LogError("Player " + player.playerID + " ping: " + player.RoundTripTime);
                    }
                }
            }
        }

#if SERVER
        public void NetworkInstantiate(GameObject obj, NetworkInstanceID netID, IPEndPoint remotePlayer)
#else
        public void NetworkInstantiate(GameObject obj, NetworkInstanceID netID, bool isLocal)
#endif
        {
            Debug.LogError("Spawning...");
            if (!spawnPrefabs.Contains(obj))
            {
                throw new ArgumentException("GameObject must be registered before it can be instantiated!", "obj");
            }
            lock (InstantiateObjects)
            {
#if SERVER
                InstantiateObjects.Enqueue(new SpawnInfo() { obj = obj, netID = netID, localPlayer = false });
#else
                InstantiateObjects.Enqueue(new SpawnInfo() { obj = obj, netID = netID, localPlayer = isLocal });
#endif
            }
#if SERVER
            SerializeCompact.BitStream bs = new SerializeCompact.BitStream();
            bs.Write(Headers.Spawn, Packet.minHeader, Packet.maxHeader);
            bs.Write(spawnPrefabs.IndexOf(obj), 0, spawnPrefabs.Count - 1);
            bs.Write(netID.Value, uint.MinValue, uint.MaxValue);
            lock (connectedPlayers)
            {
                foreach (var player in connectedPlayers)
                {
                    SerializeCompact.BitStream newbs = bs.Clone();
                    bool isLocal = remotePlayer.Equals(player.Key);
                    newbs.Write(isLocal);
                    SendDataTo(newbs, player.Key);
                    if (isLocal)
                    {
                        player.Value.netIDs.Add(netID);
                    }
                }
            }
#endif
        }
    }
}