﻿//------------------------------------------
// Author: Tim Cassell
// Created: May 09, 2016
//------------------------------------------

using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using Arq.Utilities;
using Arq.Collections.Concurrent;

namespace Arq.Networking
{
    enum Headers
    {
        Invalid,
        Connect,
        Disconnect,
        Initialize,
        Ping,
        Response,
        Input,
        Spawn,
        Destroy,
        Custom
    }

    internal struct LivingObject
    {
        public NetworkIdentity prefab;
        public NetworkIdentity obj;
    }

    public partial class NetworkManager : MonoBehaviour
    {
        static readonly Headers maxHeader;
        static readonly Headers minHeader;

        static NetworkManager()
        {
            maxHeader = SpecialFunctions.GetHighestValue<Headers>();
            minHeader = SpecialFunctions.GetLowestValue<Headers>();
        }
        struct DataPacket
        {
            public IPEndPoint destination;
            public Collections.BitStream data;

            public DataPacket(IPEndPoint destination, Collections.BitStream data)
            {
                this.destination = destination;
                this.data = data;
            }
        }

        /// <summary>
        /// Method to encrypt the 32-bit crc. Default is to XOR with PROTOCOL_ID. Set to null to not encrypt.
        /// </summary>
        public Func<uint, uint> CRCEncrypt = delegate (uint x)
        {
            return x ^ PROTOCOL_ID;
        };
        /// <summary>
        /// Method to decrypt the 32-bit crc. Default is to XOR with PROTOCOL_ID. Set to null to not decrypt.
        /// </summary>
        public Func<uint, uint> CRCDecrypt = delegate (uint x)
        {
            return x ^ PROTOCOL_ID;
        };
        /// <summary>
        /// Method to encrypt the entire data payload. Default is null to not encrypt.
        /// </summary>
        public Func<byte[], byte[]> DataEncrypt = null;
        /// <summary>
        /// Method to decrypt the entire data payload. Default is null to not decrypt.
        /// </summary>
        public Func<byte[], byte[]> DataDecrypt = null;

        public bool dontDestroyOnLoad = false;

#pragma warning disable  
        [SerializeField]
        string SERVER_IP = "127.0.0.1";
#pragma warning restore 
        [SerializeField]
        ushort SERVER_PORT = 55566;
        [SerializeField]
        int MAX_PLAYERS_PER_SERVER = 2000;
        [SerializeField]
        uint ProtocolID = 3764243738u;
        static uint PROTOCOL_ID;

        public Connection localMachine
        {
            get;
            private set;
        }
        public NetworkIdentity playerObject;
        [SerializeField]
        List<NetworkIdentity> spawnPrefabs;
        public NetworkIdentity[] SpawnPrefabs
        {
            get
            {
                return spawnPrefabs.ToArray();
            }
        }

        public bool IsConnected
        {
            get;
            private set;
        }

        public int timeoutMs = 5000;
        public int simulatedPing = 0;
        internal static int rttReping = 500;

        AsyncTwoKeyOrDictionary<IPEndPoint, NetworkInstanceID, Connection> connectedPlayers = new AsyncTwoKeyOrDictionary<IPEndPoint, NetworkInstanceID, Connection>();
        public Connection[] ConnectedPlayers
        {
            get
            {
                return connectedPlayers.Values.ToArray();
            }
        }
        /*
        Connection AddConnectedPlayer(IPEndPoint playerEndPoint, NetworkInstanceID playerID)
        {
            Connection player = new Connection(playerEndPoint, playerID);
            connectedPlayers.Add(playerEndPoint, playerID, player);
            return player;
        }
        void RemoveConnectedPlayer(IPEndPoint playerEndPoint)
        {
            connectedPlayers.RemoveKey1(playerEndPoint);
        }
        void RemoveConnectedPlayer(NetworkInstanceID playerID)
        {
            connectedPlayers.RemoveKey2(playerID);
        }
        */
#if SERVER
        HashSet<NetworkInstanceID> livingObjectIDs = new HashSet<NetworkInstanceID>();
        uint playerIDs = 0;
        NetworkInstanceID GetFreshPlayerID()
        {
            while (connectedPlayers.ContainsKey2(new NetworkInstanceID(playerIDs)) || playerIDs == 0)
            {
                playerIDs++;
            }
            return new NetworkInstanceID(playerIDs);
        }
        uint objectIDs = 0;
        NetworkInstanceID GetFreshObjectID()
        {
            while (ObjectIDExists())
            {
                objectIDs++;
            }
            return new NetworkInstanceID(objectIDs);
        }

        bool ObjectIDExists()
        {
            lock (livingObjectIDs)
            {
                return livingObjectIDs.Contains(new NetworkInstanceID(objectIDs));
            }
        }
#endif
        
        NonBlockingQueue<GameObject> DestroyObjects = new NonBlockingQueue<GameObject>();
        NonBlockingQueue<SpawnInfo> InstantiateObjects = new NonBlockingQueue<SpawnInfo>();
        BlockingPriorityQueue<DataPacket> queuedReceiveData;

        Socket sck;
        IPEndPoint ipEndPoint;
        bool listening = true;
        bool processing = true;

        void Awake()
        {
            if (FindObjectsOfType<NetworkManager>().Length == 1)
            {
                if (playerObject != null)
                {
                    spawnPrefabs.Add(playerObject);
                }
                PROTOCOL_ID = ProtocolID;
                if (dontDestroyOnLoad)
                {
                    DontDestroyOnLoad(this);
                }
            }
            else
            {
                DestroyImmediate(this);
            }
        }

        void Start()
        {
            queuedReceiveData = new BlockingPriorityQueue<DataPacket>(MAX_PLAYERS_PER_SERVER, false);
            sck = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
#if SERVER
            ipEndPoint = new IPEndPoint(IPAddress.Any, SERVER_PORT);
            sck.Bind(ipEndPoint);
            localMachine = new Connection((IPEndPoint)sck.LocalEndPoint, NetworkInstanceID.Invalid);
#else
            ipEndPoint = new IPEndPoint(IPAddress.Parse(SERVER_IP), SERVER_PORT);
            sck.Bind(new IPEndPoint(IPAddress.Any, 0));
#endif
            new Thread(() => ListenForData()).Start();
            new Thread(() => ProcessData()).Start();
            StartCoroutine(SendFinally());

#if !SERVER
            connectedPlayers.Add(ipEndPoint, NetworkInstanceID.Invalid, new Connection(ipEndPoint, NetworkInstanceID.Invalid));
            Connect();
#else
            IsConnected = true;
            StartCoroutine(EndUpdate());
#endif
        }

        void OnGUI()
        {
            int counter = 0;
            lock (connectedPlayers.SyncRoot)
            {
                foreach (var player in connectedPlayers.Values)
                {
                    GUI.Box(new Rect(0, counter, Screen.width / 4, Screen.height / 16), "Player " + player.connectionID.Value + " ping: " + player.RoundTripTime);
                    counter += Screen.height / 16 + 1;
                }
            }
        }

        // MAIN THREAD CALL ONLY
        void SpawnObject(SpawnInfo info)
        {
            NetworkIdentity NID = Instantiate(info.obj, info.position, info.rotation).GetComponent<NetworkIdentity>();
            NID.netID = info.objectID;
            NID.IsLocalPlayer = info.parent == localMachine;
            info.parent._localObjects.Add(info.objectID, new LivingObject() { prefab = info.obj, obj = NID });
#if SERVER
            lock (livingObjectIDs)
            {
                livingObjectIDs.Add(info.objectID);
            }
#endif
        }

#if SERVER
        ReaderWriterLockSlim spawnLocker = new ReaderWriterLockSlim();
#endif
        void Update()
        {
#if SERVER
            spawnLocker.EnterWriteLock();
            try
            {
#endif
                for (int count = InstantiateObjects.Count; count > 0; count--)
                {
                    var obj = InstantiateObjects.Dequeue();
                    if (obj != null)
                    {
                        SpawnObject(obj);
                    }
                }
#if SERVER
            }
            finally
            {
                spawnLocker.ExitWriteLock();
            }
#endif
            for (int count = DestroyObjects.Count; count > 0; count--)
            {
                var obj = DestroyObjects.Dequeue();
                if (obj != null)
                {
                    Destroy(obj);
                }
            }
            if (initDone)
            {
                initDone = false;
                IsConnected = true;
                StartCoroutine(EndUpdate());
            }
        }

        IEnumerator EndUpdate()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();
                
                for (int count = scriptSerializers.Count; count > 0; count--)
                {
                    var obj = scriptSerializers.Dequeue();
                    if (obj != null)
                    {
                        DeSerializeScripts(new Collections.BitSerializer(obj));
                    }
                }
                var data = SerializeScripts(localMachine);
                if (data != null)
                {
#if SERVER
                    SendDataToAll(data);
#else
                    SendData(data);
#endif
                }

                Collections.BitStream bs = new Collections.BitStream();
                bs.Write(Headers.Ping, minHeader, maxHeader);
                lock (connectedPlayers.SyncRoot)
                {
#if SERVER
                    var disconnects = new List<Connection>();
                    foreach (var player in connectedPlayers.Values)
                    {
                        if (player.RoundTripTime > timeoutMs) // if it's been longer than timeout since a message was received
                        {
                            Debug.LogError("Ping time-out!");
                            disconnects.Add(player);
                        }
                        else // serialize NetworkBehaviour scripts
                        {
                            data = SerializeScripts(player);
                            if (data != null)
                            {
                                SendDataToAll(data);
                            }
                            else if (player.QueuedSendData.Count == 0)
                            //if (player.RoundTripTime > rttReping / 2 || player.TimeSinceLastPacketSent > rttReping / 2)
                            {
                                SendDataTo(bs, player.connectionEndPoint);
                            }
                        }
                    }
                    foreach (var player in disconnects)
                    {
                        Disconnect(player);
                    }
#else
                    foreach (var player in connectedPlayers.Values)
                    {
                        // serialize NetworkBehaviour scripts
                        data = SerializeScripts(player);
                        if (data != null)
                        {
                            SendData(data);
                        }
                        else if (player.QueuedSendData.Count == 0)
                        //if (player.RoundTripTime > rttReping / 2 || player.TimeSinceLastPacketSent > rttReping / 2)
                        {
                            SendData(bs);
                        }
                    }
#endif
                }
            }
        }

#if SERVER
        void Disconnect(Connection player)
        {
            lock (connectedPlayers.SyncRoot)
            {
                if (!connectedPlayers.ContainsKey1(player.connectionEndPoint))
                {
                    return;
                }
                connectedPlayers.RemoveKey1(player.connectionEndPoint);
            }
            var destroys = new List<LivingObject>();
            lock (player._localObjects.SyncRoot)
            {
                foreach (var obj in player._localObjects.Values)
                {
                    destroys.Add(obj);
                }
                foreach (var obj in destroys)
                {
                    NetworkDestroy(obj.obj, player);
                }       
            }
        }
#else
        public void Connect()
        {
            Arq.Collections.BitStream bs = new Arq.Collections.BitStream();
            bs.Write(Headers.Connect, minHeader, maxHeader);
            SendData(bs);
        }

        public void Disconnect()
        {
            Arq.Collections.BitStream bs = new Arq.Collections.BitStream();
            bs.Write(Headers.Disconnect, minHeader, maxHeader);
            SendData(bs);
            IsConnected = false;
        }
#endif
        void OnApplicationQuit()
        {
            OnDestroy();
        }

        void OnDestroy()
        {
#if !SERVER
            Disconnect();
#else
            Collections.BitStream bs = new Collections.BitStream();
            bs.Write(Headers.Disconnect, minHeader, maxHeader);
            SendDataToAll(bs);
#endif
            SendDataImmediate();

            listening = false;
            processing = false;
            sck.Close();
        }



#if SERVER
        object spawnLock = new object();
        object destroyLock = new object();

        /// <summary>
        /// Spawn an object.
        /// </summary>
        /// <param name="obj">The object to be spawned.</param>
        /// <param name="player">The player for the object to be attached to (null for none/server assignment)</param>
        public void NetworkInstantiate(NetworkIdentity obj, Vector3 position, Quaternion rotation, Connection player = null)
        {
            NetworkInstantiate(obj, position, rotation, GetFreshObjectID(), (player == null) ? localMachine : player);
        }

        void NetworkInstantiate(NetworkIdentity obj, Vector3 position, Quaternion rotation, NetworkInstanceID netID, Connection remotePlayer)
        {
            if (!spawnPrefabs.Contains(obj))
            {
                throw new ArgumentException("GameObject must be registered before it can be instantiated!", "obj");
            }

            Collections.BitStream bs = new Collections.BitStream();
            bs.Write(Headers.Spawn, minHeader, maxHeader);
            bs.Write(spawnPrefabs.IndexOf(obj), 0, spawnPrefabs.Count - 1);
            bs.Write(netID.Value, uint.MinValue, uint.MaxValue);
            Collections.BitSerializer bser = new Collections.BitSerializer(bs, false);
            bser.Serialize(ref position, SpecialFunctions.MinVector3, SpecialFunctions.MaxVector3, 7);
            Vector3 rot = rotation.eulerAngles;
            bser.Serialize(ref rot, SpecialFunctions.MinVector3, SpecialFunctions.MaxVector3, 7);
            lock (spawnLock)
            {
                InstantiateObjects.Enqueue(new SpawnInfo() { parent = remotePlayer, obj = obj, objectID = netID, position = position, rotation = rotation });
                lock (connectedPlayers.SyncRoot)
                {
                    foreach (var player in connectedPlayers.Values)
                    {
                        Collections.BitStream newbs = bs.Clone();
                        bool isLocal = remotePlayer == player;
                        newbs.Write(isLocal);
                        SendDataTo(newbs, player.connectionEndPoint, -1);
                    }
                }
            }
        }
#else
        void LocalInstantiate(NetworkIdentity obj, Vector3 position, Quaternion rotation, NetworkInstanceID netID, bool isLocal)
        {
            if (!spawnPrefabs.Contains(obj))
            {
                throw new ArgumentException("GameObject must be registered before it can be instantiated!", "obj");
            }
            InstantiateObjects.Enqueue(new SpawnInfo() { obj = obj, objectID =  netID, parent = (isLocal) ? localMachine : connectedPlayers[ipEndPoint], position = position, rotation = rotation });
        }
#endif
        /// <summary>
        /// Spawn an object.
        /// </summary>
        /// <param name="obj">The object to be destroyed.</param>
        /// <param name="player">The player the object is attached to</param>
#if SERVER
        public
#endif
            void NetworkDestroy(NetworkIdentity obj, Connection player)
        {
#if SERVER
            Collections.BitStream bs = new Collections.BitStream();
            bs.Write(Headers.Destroy, minHeader, maxHeader);
            bs.Write(player == localMachine);
            bs.Write(obj.netID.Value, uint.MinValue, uint.MaxValue);

            lock (destroyLock)
            {
                SendDataToAll(bs, -1);

                player._localObjects.Remove(obj.netID);
                lock (livingObjectIDs)
                {
                    if (livingObjectIDs.Contains(obj.netID))
                    {
                        livingObjectIDs.Remove(obj.netID);
                    }
                }
                DestroyObjects.Enqueue(obj._go);
            }
#else
            player._localObjects.Remove(obj.netID);
            DestroyObjects.Enqueue(obj._go);
#endif
        }
    }
}