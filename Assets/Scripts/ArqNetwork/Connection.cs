using System;
using System.Net;
using System.Collections.Generic;
using Arq.Collections;
using Arq.Collections.Concurrent;

namespace Arq.Networking
{
    public class Connection
    {
        internal class FragmentedPackets
        {
            class FragRatio
            {
                public BitStream[] bitStreams;
                public uint count;
            }

            Dictionary<uint, FragRatio> fragments = new Dictionary<uint, FragRatio>();

            /// <summary>
            /// add a fragment to the collection of fragments
            /// </summary>
            /// <param name="sequence"></param>
            /// <param name="fragCount"></param>
            /// <param name="id"></param>
            /// <param name="bitStream"></param>
            public void Add(uint sequence, byte fragCount, byte id, BitStream bitStream)
            {
                lock (fragments)
                {
                    if (fragments.ContainsKey(sequence))
                    {
                        var frags = fragments[sequence];
                        if (frags.bitStreams[id] == null)
                        {
                            frags.bitStreams[id] = bitStream;
                            frags.count++;
                        }
                    }
                    else
                    {
                        var frags = new BitStream[fragCount];
                        frags[id] = bitStream;
                        fragments.Add(sequence, new FragRatio() { bitStreams = frags, count = 1 });
                    }
                }
            }

            /// <summary>
            /// returns a full packet, or null if there are none
            /// </summary>
            /// <returns></returns>
            public BitStream GetNextComplete(uint sequence)
            {
                lock (fragments)
                {
                    var frags = fragments[sequence].bitStreams;
                    if (frags.Length == fragments[sequence].count)
                    {
                        for (int j = 1; j < frags.Length; j++)
                        {
                            frags[0].Merge(frags[j]);
                        }
                        fragments.Remove(sequence);
                        return frags[0];
                    }
                    return null;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="currentSequence"></param>
            /// <param name="stepsBack"></param>
            public void RemoveOld(uint currentSequence, int stepsBack)
            {
                List<uint> removeKeys = new List<uint>();
                lock (fragments)
                {
                    foreach (var key in fragments.Keys)
                    {
                        if (key < currentSequence - stepsBack)
                        {
                            removeKeys.Add(key);
                        }
                    }
                    foreach (var key in removeKeys)
                    {
                        fragments.Remove(key);
                    }
                }
            }
        }

        BlockingPriorityQueue<BitStream> queuedSendData = new BlockingPriorityQueue<BitStream>();
        public NetworkInstanceID connectionID
        {
            get;
            internal set;
        }
        internal IPEndPoint connectionEndPoint;
        CircularInt16 remotePacketID = -1;
        uint remotePacketBitField = 0;
        NonBlockingQueue<DateTime> pingCounters = new NonBlockingQueue<DateTime>();
        DateTime lastPacketSent;
        int oldPing = 0;
        internal FragmentedPackets fragmentedPackets = new FragmentedPackets();
        internal AsyncDictionary<NetworkInstanceID, LivingObject> _localObjects = new AsyncDictionary<NetworkInstanceID, LivingObject>();
        public UnityEngine.GameObject[] localObjects
        {
            get
            {
                lock (_localObjects.SyncRoot)
                {
                    UnityEngine.GameObject[] objs = new UnityEngine.GameObject[_localObjects.Count];
                    int i = 0;
                    foreach (var obj in _localObjects.Values)
                    {
                        objs[i] = obj.obj._go;
                        i++;
                    }
                    return objs;
                }
            }
        } 

        internal BlockingPriorityQueue<BitStream> QueuedSendData
        {
            get
            {
                return queuedSendData;
            }
        }

        internal ushort RemotePacketID
        {
            get
            {
                return remotePacketID.ToUInt16();
            }
            set
            {
                CircularInt16 CI = value;
                if (CI > remotePacketID)
                {
                    int dif = (CI - remotePacketID).ToInt16();
                    remotePacketBitField <<= dif;
                    remotePacketBitField |= (1u << (dif - 1));
                    remotePacketID = CI;
                }
                else
                {
                    int dif = (remotePacketID - CI).ToInt16();
                    remotePacketBitField |= (1u << (dif - 1));
                }
            }
        }

        internal int TimeSinceLastPacketSent
        {
            get
            {
                return (int)(DateTime.Now - lastPacketSent).TotalMilliseconds;
            }
        }

        internal void ResetPacketTimer()
        {
            lastPacketSent = DateTime.Now;
        }

        internal ushort LocalPacketID
        {
            get;
            set;
        }

        internal uint RemotePacketBitField
        {
            get
            {
                return remotePacketBitField;
            }
        }

        internal bool RemotePacketFound(ushort packetID)
        {
            CircularInt16 CI = packetID;
            if (CI > remotePacketID || (remotePacketID - CI).ToInt16() > 32)
            {
                return false;
            }
            if (CI == remotePacketID)
            {
                return true;
            }
            return (remotePacketBitField | (1u << ((remotePacketID - CI).ToInt16() - 1))) > 0;
        }

        /// <summary>
        /// The current ping time of the connection (in milliseconds)
        /// </summary>
        public int RoundTripTime
        {
            get
            {
                lock (pingCounters.SyncRead)
                {
                    if (pingCounters.Count > 0)
                    {
                        int curPing = (int)(DateTime.Now - pingCounters.Peek()).TotalMilliseconds;
                        return (oldPing > curPing /*|| curPing < NetworkManager.rttReping*/) ? oldPing : curPing;
                    }
                }
                return oldPing;
            }
        }

        internal Connection(IPEndPoint connectionEndPoint, NetworkInstanceID connectionID)
        {
            this.connectionEndPoint = connectionEndPoint;
            this.connectionID = connectionID;
        }

        CircularInt16 newestSentPacket;

        /// <summary>
        /// Start measuring the ping
        /// </summary>
        internal void StartRTTMeasure(ushort packetID)
        {
            newestSentPacket = packetID;
            pingCounters.Enqueue(DateTime.Now);
        }

        /// <summary>
        /// Stop the ping measurement
        /// </summary>
        internal void StopRTTMeasure(ushort packetID)
        {
            lock (pingCounters.SyncRead)
            {
                CircularInt16 count = newestSentPacket - packetID;
                while (count.ToInt16() < pingCounters.Count)
                {
                    oldPing = (int)(DateTime.Now - pingCounters.Dequeue()).TotalMilliseconds;
                }
            }
        }
    }
}