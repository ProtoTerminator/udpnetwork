using UnityEngine;

namespace Arq.Networking
{
    internal class SpawnInfo
    {
        public Connection parent;
        public NetworkIdentity obj;
        public NetworkInstanceID objectID;
        public Vector3 position;
        public Quaternion rotation;
    }
}