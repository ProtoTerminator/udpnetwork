using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System;
using Arq.Utilities;
using Arq.Collections.Concurrent;

namespace Arq.Networking
{
    public partial class NetworkManager
    {
        NonBlockingQueue<Arq.Collections.BitStream> scriptSerializers = new NonBlockingQueue<Arq.Collections.BitStream>();

        bool DeSerializeScripts(Arq.Collections.BitSerializer bitSerializer)
        {
            uint id = 0;
            bitSerializer.Serialize(ref id, uint.MinValue, uint.MaxValue);
            NetworkInstanceID netID = new NetworkInstanceID(id);

            if (localMachine.connectionID == netID)
            {
                while (bitSerializer.Serialize(ref id, uint.MinValue, uint.MaxValue))
                {
                    netID = new NetworkInstanceID(id);
                    if (!localMachine._localObjects.ContainsKey(netID))
                    {
                        return false;
                    }
                    if (!DeSerialize(localMachine._localObjects[netID].obj, bitSerializer))
                    {
                        return false;
                    }
                }
            }
            else
            {
                if (!connectedPlayers.ContainsKey2(netID))
                {
                    return false;
                }
                var objects = connectedPlayers[netID]._localObjects;
                while (bitSerializer.Serialize(ref id, uint.MinValue, uint.MaxValue))
                {
                    netID = new NetworkInstanceID(id);
                    if (!DeSerialize(objects[netID].obj, bitSerializer))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        bool DeSerialize(NetworkIdentity ID, Arq.Collections.BitSerializer bitSerializer)
        {
            foreach(var script in ID.NetBehaviours)
            {
                if (!script.Serialize(bitSerializer))
                {
                    return false;
                }
            }
            return true;
        }

        void ListenForData()
        {
            EndPoint endPoint = ipEndPoint;
            while (listening)
            {
                //Debug.Log("listening...");
                byte[] buffer = new byte[512];
                try
                {
                    int rec = sck.ReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref endPoint);
                    //Debug.Log("received message");
                    if (DataEncrypt != null && DataDecrypt != null)
                    {
                        Array.Resize(ref buffer, rec);
                        buffer = DataDecrypt(buffer);
                        queuedReceiveData.Enqueue(new DataPacket((IPEndPoint)endPoint, new Arq.Collections.BitStream(buffer)), 0);
                    }
                    else
                    {
                        Arq.Collections.BitStream bs = new Arq.Collections.BitStream(rec * 8);
                        bs.Write(buffer, 0, rec);
                        queuedReceiveData.Enqueue(new DataPacket((IPEndPoint)endPoint, bs), 0);
                    }
                    //Debug.LogError("queue size: " + queuedReceiveData.Count);
                }
                catch (SocketException e)
                {
                    Debug.LogError(e.Message + "; " + e.ErrorCode);
                }
            }
        }

        void ProcessData()
        {
            DataPacket rcv;
            IPEndPoint ep;
            Arq.Collections.BitStream packet = new Arq.Collections.BitStream();
            uint crc = 0;
            uint crcRead;
            sbyte importance;
            byte fragCount;
            byte fragIdPacketCount;
            ushort sequence;
            ushort ack;
            uint ackBitfield;
            long bits;
            Headers header = Headers.Invalid;

            while (processing)
            {
                //Debug.LogError("data after processing: " + queuedReceiveData.Count);
                rcv = queuedReceiveData.Dequeue(); // blocks until queue has >1 item
                //Debug.LogError("data before processing: " + queuedReceiveData.Count);
                //Debug.Log("received");
                packet = rcv.data;
                ep = rcv.destination;

                bits = packet.StoredBits;

                packet.Read(out crc, uint.MinValue, uint.MaxValue);
                crcRead = CRC32.GenerateCrc32(packet.GetByteArray());
                if (CRCEncrypt != null && CRCDecrypt != null)
                {
                    crcRead = CRCDecrypt(crcRead);
                }

                if (crc != crcRead)
                {
                    Debug.LogError("CRC mismatch! Aborting packet read." + "\nbits: " + bits);
                    continue;
                }
                packet.Read(out importance, sbyte.MinValue, sbyte.MaxValue);
                packet.Read(out fragCount, byte.MinValue, byte.MaxValue);
                packet.Read(out fragIdPacketCount, byte.MinValue, byte.MaxValue);
                packet.Read(out sequence, ushort.MinValue, ushort.MaxValue);

                Connection player = null;

                if (connectedPlayers.ContainsKey1(ep))
                {
                    player = connectedPlayers[ep];
                    if (player.RemotePacketFound(sequence)) // skip any duplicate packets
                    {
                        //Debug.LogError("skipping duplicate, sequence: " + sequence);
                        continue;
                    }
                    player.RemotePacketID = sequence;
                    if (fragCount > 1)
                    {
                        player.fragmentedPackets.Add(sequence, fragCount, fragIdPacketCount, packet);
                        packet = player.fragmentedPackets.GetNextComplete(sequence);
                        player.fragmentedPackets.RemoveOld(sequence, 64);
                        if (packet == null)
                        {
                            continue;
                        }
                        fragIdPacketCount = 1;
                    }
                }

                for (; fragIdPacketCount > 0; fragIdPacketCount--)
                {
                    // TODO: add new dictionary for important packets, check against ack and ackbitfield
                    packet.Read(out ack, ushort.MinValue, ushort.MaxValue);
                    packet.Read(out ackBitfield, uint.MinValue, uint.MaxValue);

                    if (player != null)
                    {
                        player.StopRTTMeasure(ack);
                    }

                    if (!ProcessPacket(packet, player, ep, out header))
                    {
                        Debug.LogError("There is an issue with the packet. Aborting read. Header: " + header);
                        break;
                    }
                }
            }
        }

        int initSpawns = 0;
        bool initDone = false;

        bool ProcessPacket(Arq.Collections.BitStream packet, Connection player, IPEndPoint ep, out Headers header)
        {
            uint protocol;
            if (!packet.Read(out header, minHeader, maxHeader))
            {
                return false;
            }
            //Debug.Log("header: " + header);

            switch (header)
            {
                case Headers.Connect:
                    {
#if SERVER
                        if (!packet.Read(out protocol, uint.MinValue, uint.MaxValue) ||
                            protocol != PROTOCOL_ID)
                        {
                            return false;
                        }
                        if (player == null)
                        {
                            // add player
                            player = new Connection(ep, GetFreshPlayerID());

                            Collections.BitStream newPacket = new Collections.BitStream();

                            uint playerID = player.connectionID.Value;
                            
                            int objCount;
                            lock (spawnLock)
                            {
                                lock (destroyLock)
                                {
                                    spawnLocker.EnterReadLock();
                                    try
                                    {
                                        connectedPlayers.Add(player.connectionEndPoint, player.connectionID, player);
                                        // spawn existing objects on connecting client
                                        objCount = localMachine._localObjects.Count;
                                        foreach (var item in connectedPlayers.Values)
                                        {
                                            objCount += item._localObjects.Count;
                                        }
                                        if (objCount > 0)
                                        {
                                            foreach (var obj in localMachine._localObjects.Values)
                                            {
                                                newPacket.Clear();
                                                newPacket.Write(Headers.Initialize, minHeader, maxHeader);
                                                newPacket.Write(playerID, uint.MinValue, uint.MaxValue);
                                                newPacket.Write(objCount, 0, int.MaxValue);
                                                //change to spawn
                                                newPacket.Write(spawnPrefabs.IndexOf(obj.prefab), 0, spawnPrefabs.Count);
                                                newPacket.Write(obj.obj.netID.Value, uint.MinValue, uint.MaxValue);
                                                Collections.BitSerializer bser = new Collections.BitSerializer(newPacket, false);
                                                Vector3 position = obj.obj._position;
                                                Vector3 rotation = obj.obj._rotation.eulerAngles;
                                                bser.Serialize(ref position, SpecialFunctions.MinVector3, SpecialFunctions.MaxVector3, 7);
                                                bser.Serialize(ref rotation, SpecialFunctions.MinVector3, SpecialFunctions.MaxVector3, 7);
                                                newPacket.Write(false);
                                                SendDataTo(newPacket, ep, -1);
                                            }
                                            foreach (var item in connectedPlayers.Values)
                                            {
                                                lock (item._localObjects.SyncRoot)
                                                {
                                                    foreach (var obj in item._localObjects.Values)
                                                    {
                                                        newPacket.Clear();
                                                        newPacket.Write(Headers.Initialize, minHeader, maxHeader);
                                                        newPacket.Write(playerID, uint.MinValue, uint.MaxValue);
                                                        newPacket.Write(objCount, 0, int.MaxValue);
                                                        //change to spawn
                                                        newPacket.Write(spawnPrefabs.IndexOf(obj.prefab), 0, spawnPrefabs.Count);
                                                        newPacket.Write(obj.obj.netID.Value, uint.MinValue, uint.MaxValue);
                                                        Collections.BitSerializer bser = new Collections.BitSerializer(newPacket, false);
                                                        Vector3 position = obj.obj._position;
                                                        Vector3 rotation = obj.obj._rotation.eulerAngles;
                                                        bser.Serialize(ref position, SpecialFunctions.MinVector3, SpecialFunctions.MaxVector3, 7);
                                                        bser.Serialize(ref rotation, SpecialFunctions.MinVector3, SpecialFunctions.MaxVector3, 7);
                                                        newPacket.Write(false);
                                                        SendDataTo(newPacket, ep, -1);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    finally
                                    {
                                        spawnLocker.ExitReadLock();
                                    }
                                }
                            }
                            if (objCount == 0)
                            {
                                newPacket.Clear();
                                newPacket.Write(Headers.Initialize, minHeader, maxHeader);
                                newPacket.Write(playerID, uint.MinValue, uint.MaxValue);
                                newPacket.Write(0, 0, int.MaxValue);
                                SendDataTo(newPacket, ep, -1);
                            }

                            if (playerObject != null)
                            {
                                NetworkInstantiate(playerObject, Vector3.up * 5, Quaternion.identity, player);
                            }
                        }
#else
                        // Someone is attempting to connect directly
                        //return false;
#endif
                        break;
                    }
                case Headers.Disconnect:
                    {
                        if (!packet.Read(out protocol, uint.MinValue, uint.MaxValue) ||
                            protocol != PROTOCOL_ID)
                        {
                            return false;
                        }
#if SERVER
                        Disconnect(player);
#else
                        // server went offline!
                        IsConnected = false;
#endif
                        break;
                    }
                case Headers.Initialize:
                    {
                        uint playerID;
                        int objCount;
                        if (!packet.Read(out playerID, uint.MinValue, uint.MaxValue) ||
                            !packet.Read(out objCount, 0, int.MaxValue))
                        {
                            return false;
                        }
                        if (objCount == 0)
                        {
                            if (!packet.Read(out protocol, uint.MinValue, uint.MaxValue) ||
                                protocol != PROTOCOL_ID)
                            {
                                return false;
                            }
                            if (localMachine == null)
                            {
                                localMachine = new Connection((IPEndPoint)sck.LocalEndPoint, new NetworkInstanceID(playerID));
                            }

                            initDone = true;
                            break;
                        }
                        else
                        {
                            initSpawns++;
                            if (initSpawns >= objCount)
                            {
                                initDone = true;
                            }
                            if (localMachine == null)
                            {
                                localMachine = new Connection((IPEndPoint)sck.LocalEndPoint, new NetworkInstanceID(playerID));
                            }

                            goto case Headers.Spawn;
                        }
                    }
                case Headers.Spawn:
                    {
                        // Instantiate gameobject
#if SERVER
                        // error, server should only send spawn packets
                        return false;
#else
                        int index;
                        uint netID;
                        Vector3 position = Vector3.zero;
                        Vector3 rotation = Vector3.zero;
                        bool localPlayer;
                        Collections.BitSerializer bser = new Collections.BitSerializer(packet, true);
                        if (!packet.Read(out index, 0, spawnPrefabs.Count - 1) ||
                            !packet.Read(out netID, uint.MinValue, uint.MaxValue) ||
                            !bser.Serialize(ref position, SpecialFunctions.MinVector3, SpecialFunctions.MaxVector3, 7) ||
                            !bser.Serialize(ref rotation, SpecialFunctions.MinVector3, SpecialFunctions.MaxVector3, 7) ||
                            !packet.Read(out localPlayer) ||
                            !packet.Read(out protocol, uint.MinValue, uint.MaxValue) ||
                            protocol != PROTOCOL_ID)
                        {
                            return false;
                        }
                        Quaternion rot = Quaternion.Euler(rotation);
                        try
                        {
                            LocalInstantiate(spawnPrefabs[index], position, rot, new NetworkInstanceID(netID), localPlayer);
                        }
                        catch (Exception e)
                        {
                            Debug.LogError(e.Message);
                        }
                        //Debug.LogError("instantiating object");
                        break;
#endif
                    }
                case Headers.Destroy:
                    {
                        // Destroy gameobject
#if SERVER
                        // error, server should only send destroy packets
                        return false;
#else
                        bool local;
                        uint netID;
                        if (!packet.Read(out local) ||
                            !packet.Read(out netID, uint.MinValue, uint.MaxValue) ||
                            !packet.Read(out protocol, uint.MinValue, uint.MaxValue) ||
                            protocol != PROTOCOL_ID)
                        {
                            
                            return false;
                        }
                        
                        if (local)
                        {
                            NetworkDestroy(localMachine._localObjects[new NetworkInstanceID(netID)].obj, localMachine);
                        }
                        else
                        {
                            NetworkDestroy(player._localObjects[new NetworkInstanceID(netID)].obj, player);
                        }
                        break;
#endif
                    }
                case Headers.Ping:
                    {
                        if (!packet.Read(out protocol, uint.MinValue, uint.MaxValue) ||
                            protocol != PROTOCOL_ID)
                        {
                            return false;
                        }

                        var newPacket = new Arq.Collections.BitStream();
                        newPacket.Write(Headers.Response, minHeader, maxHeader);
                        SendDataTo(newPacket, ep);
                        break;
                    }
                case Headers.Response:
                    {
                        if (!packet.Read(out protocol, uint.MinValue, uint.MaxValue) ||
                            protocol != PROTOCOL_ID)
                        {
                            return false;
                        }
                        // do nothing
                        break;
                    }
                case Headers.Custom:
                    {
                        long bits = 0;
                        if (!packet.Read(out bits, 0, long.MaxValue))
                        {
                            return false;
                        }
                        Arq.Collections.BitStream bs = new Arq.Collections.BitStream(bits);
                        bs.Merge(packet, bits);
                        
                        if (!packet.Read(out protocol, uint.MinValue, uint.MaxValue) ||
                            protocol != PROTOCOL_ID)
                        {
                            return false;
                        }
                        
                        scriptSerializers.Enqueue(bs);
                        break;
                    }
                default:
                    {
                        // Invalid header
                        return false;
                    }
            }
            return true;
        }
    }
}