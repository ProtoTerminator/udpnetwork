using System;
using Arq.Collections;
using UnityEngine;

namespace Arq.Networking
{
    [AddComponentMenu("")]
    [RequireComponentOnRoot(typeof(NetworkIdentity), singleton = true)]
    public abstract class NetworkBehaviour : MonoBehaviour
    {
        public NetworkInstanceID netID
        {
            get
            {
                return transform.root.GetComponent<NetworkIdentity>().netID;
            }
        }

        public bool IsLocalPlayer
        {
            get
            {
                return transform.root.GetComponent<NetworkIdentity>().IsLocalPlayer;
            }
        }

        public virtual void OnConnected()
        {

        }

        public virtual void OnDisconnected()
        {

        }

        public virtual void OnConnectionFailed()
        {

        }

        /// <summary>
        /// Use this method to send and receive arbitrary data over the network.
        /// </summary>
        /// <param name="bitSerializer"></param>
        /// <returns>true if it received data without errors</returns>
        public abstract bool Serialize(BitSerializer bitSerializer);
    }
}