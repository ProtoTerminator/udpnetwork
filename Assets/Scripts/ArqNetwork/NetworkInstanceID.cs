using System;
using Arq.Collections;

namespace Arq.Networking
{
	/// <summary>
	/// This is used to identify networked objects across all participants of a network.
	/// It is assigned at runtime by the server when an object is spawned.
	/// </summary>
	public struct NetworkInstanceID : ISerializable
	{
		/// <summary>
		/// A static invalid NetworkInstanceId that can be used for comparisons.
		/// </summary>
		public static readonly NetworkInstanceID Invalid = new NetworkInstanceID(0);

		/// <summary>
		/// The internal value of this identifier.
		/// </summary>
		public uint Value
		{
			get;
			private set;
		}

		public NetworkInstanceID(uint value)
		{
			Value = value;
		}

		public override bool Equals(object obj)
		{
			return obj != null && 
                obj.GetType() == typeof(NetworkInstanceID) && 
                this == (NetworkInstanceID) obj;
		}
		public override int GetHashCode()
		{
			return (int) Value;
		}

		/// <summary>
		/// Returns true if the value of the NetworkInstanceId is zero.
		/// </summary>
		/// <returns>True if zero.</returns>
		public bool IsEmpty()
		{
			return Value == 0;
		}
		
		/// <summary>
		/// Returns a string of "NetID: value".
		/// </summary>
		/// <returns>String representation of this object.</returns>
		public override string ToString()
		{
			return "NetID: " + Value;
		}

        public static bool operator ==(NetworkInstanceID c1, NetworkInstanceID c2)
		{
            if (ReferenceEquals(c1, c2))
            {
                return true;
            }
            if (((object) c1 == null) || ((object) c2 == null))
            {
                return false;
            }
            return c1.Value == c2.Value;
		}
		public static bool operator !=(NetworkInstanceID c1, NetworkInstanceID c2)
		{
			return !(c1.Value == c2.Value);
		}

        public bool Serialize(BitSerializer bitSerializer)
        {
            if (bitSerializer.IsWriting)
            {
                uint id = Value;
                bitSerializer.Serialize(ref id, uint.MinValue, uint.MaxValue);
            }
            return true;
        }
	}
}