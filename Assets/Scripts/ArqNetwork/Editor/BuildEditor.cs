using UnityEditor;
using UnityEngine;
using System.Collections;
using System;

[InitializeOnLoad]
public static class BuildEditor
{
    public const string clientMenu = "Build/Client";
    public const string serverMenu = "Build/Server";
    static string buildLoc = string.Empty;

    static BuildEditor()
    {
        EditorApplication.delayCall += Update;
    }
    static void Update()
    {
        foreach (BuildTargetGroup buildTarget in System.Enum.GetValues(typeof(BuildTargetGroup)))
        {
            if (buildTarget == 0 || buildTarget == (BuildTargetGroup)5 || buildTarget == (BuildTargetGroup)6 || buildTarget == (BuildTargetGroup)15 || buildTarget == (BuildTargetGroup)16)
            {
                continue;
            }
            if (PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTarget).Contains("SERVER"))
            {
                Debug.Log("is server");
                Menu.SetChecked(serverMenu, true);
                Menu.SetChecked(clientMenu, false);
                break;
            }
            else
            {
                Debug.Log("is client");
                Menu.SetChecked(serverMenu, false);
                Menu.SetChecked(clientMenu, true);
                break;
            }
        }
    }

    [MenuItem(serverMenu)]
    public static void ChangeToServer()
    {
        if (Menu.GetChecked(serverMenu))
        {
            return;
        }
        Menu.SetChecked(serverMenu, true);
        Menu.SetChecked(clientMenu, false);
        foreach (BuildTargetGroup buildTarget in System.Enum.GetValues(typeof(BuildTargetGroup)))
        {
            if (buildTarget == 0 || buildTarget == (BuildTargetGroup)5 || buildTarget == (BuildTargetGroup)6 || buildTarget == (BuildTargetGroup)15 || buildTarget == (BuildTargetGroup)16)
            {
                continue;
            }
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTarget, PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTarget).Replace("SERVER", string.Empty) + ";SERVER");
        }
    }

    [MenuItem(clientMenu)]
    public static void ChangeToClient()
    {
        if (Menu.GetChecked(clientMenu))
        {
            return;
        }
        Menu.SetChecked(serverMenu, false);
        Menu.SetChecked(clientMenu, true);
        foreach (BuildTargetGroup buildTarget in System.Enum.GetValues(typeof(BuildTargetGroup)))
        {
            if (buildTarget == 0 || buildTarget == (BuildTargetGroup)5 || buildTarget == (BuildTargetGroup)6 || buildTarget == (BuildTargetGroup)15 || buildTarget == (BuildTargetGroup)16)
            {
                continue;
            }
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTarget, PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTarget).Replace("SERVER", string.Empty));
        }
    }

    [MenuItem("Build/Build Dev")]
    public static void BuildDev()
    {
        BuildGame(false);
    }

    [MenuItem("Build/Build Release")]
    public static void BuildRelease()
    {
        BuildGame(true);
    }

    static void BuildGame(bool release)
    {
        // Get filename.
        buildLoc = EditorUtility.SaveFolderPanel("Choose Location of Built Game", EditorPrefs.GetString("build"), "");
        if (buildLoc == string.Empty)
        {
            return;
        }
        EditorPrefs.SetString("build", buildLoc);
        string[] levels = new string[EditorBuildSettings.scenes.Length];
        for (int i = 0; i < levels.Length; i++)
        {
            levels[i] = EditorBuildSettings.scenes[i].path;
        }

        bool isServer = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildPipeline.GetBuildTargetGroup(EditorUserBuildSettings.activeBuildTarget)).Contains("SERVER");

        ChangeToServer();
        BuildPipeline.BuildPlayer(levels, buildLoc + "/Server.exe", EditorUserBuildSettings.activeBuildTarget, release ? BuildOptions.None : BuildOptions.Development);
        ChangeToClient();
        BuildPipeline.BuildPlayer(levels, buildLoc + "/Client.exe", EditorUserBuildSettings.activeBuildTarget, release ? BuildOptions.None : BuildOptions.Development);

        if (isServer)
        {
            ChangeToServer();
        }


        // Run the game (Process class from System.Diagnostics).
        /*Process proc = new Process();
        proc.StartInfo.FileName = path + "BuiltGame.exe";
        proc.Start();*/
    }
}