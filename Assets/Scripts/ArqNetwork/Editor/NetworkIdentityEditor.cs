using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Arq.Networking
{
    [CustomEditor(typeof(NetworkIdentity))]
    public class NetworkIdentityEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            NetworkIdentity targetNI = (NetworkIdentity) target;

            DrawDefaultInspector();

            EditorGUILayout.LabelField("Net ID", targetNI.netID.Value.ToString());
            EditorGUILayout.Toggle("Is Local Player", targetNI.IsLocalPlayer);
        }
    }
}