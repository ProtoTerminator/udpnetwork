using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Arq.Networking
{
    [CustomEditor(typeof(NetworkBehaviour))]
    public class NetworkBehaviourEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            //NetworkBehaviour targetNB = (NetworkBehaviour) target;

            DrawDefaultInspector();
        }
    }
}