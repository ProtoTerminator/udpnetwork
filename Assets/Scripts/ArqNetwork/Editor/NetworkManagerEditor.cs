using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Reflection;

namespace Arq.Networking
{
    [CustomEditor(typeof(NetworkManager))]
    public class NetworkManagerEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            NetworkManager targetNM = (NetworkManager) target;

            DrawDefaultInspector();

            if (targetNM.playerObject != null)
            {
                if (PrefabUtility.GetPrefabType(targetNM.playerObject) != PrefabType.Prefab)
                {
                    Debug.LogError(targetNM.playerObject + " could not be added as spawnable as it is not a prefab.");
                    targetNM.playerObject = null;
                }
            }

            var prefabs = typeof(NetworkManager).GetField("spawnPrefabs", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(targetNM) as System.Collections.Generic.List<NetworkIdentity>;
            for (int i = 0; i < prefabs.Count; i++)
            {
                if (prefabs[i] != null)
                {
                    if (PrefabUtility.GetPrefabType(prefabs[i]) != PrefabType.Prefab)
                    {
                        Debug.LogError(prefabs[i].name + " could not be added as spawnable as it is not a prefab.");

                        prefabs[i] = null;
                    }
                }
            }
        }
    }
}