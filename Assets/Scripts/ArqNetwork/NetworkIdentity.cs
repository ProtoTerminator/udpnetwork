using UnityEngine;
using System.Collections;

namespace Arq.Networking
{
    [DisallowMultipleComponent]
    public sealed class NetworkIdentity : MonoBehaviour
    {
        internal NetworkBehaviour[] NetBehaviours;
        internal GameObject _go;
        internal Vector3 _position;
        internal Quaternion _rotation;

        public NetworkInstanceID netID
        {
            get;
            internal set;
        }
        public bool IsLocalPlayer
        {
            get;
            internal set;
        }

        void Update()
        {
            _position = transform.position;
            _rotation = transform.rotation;
        }

        void Awake()
        {
            init();
        }

        internal void init()
        {
            _go = gameObject;
            NetBehaviours = GetComponentsInChildren<NetworkBehaviour>();
        }
    }
}