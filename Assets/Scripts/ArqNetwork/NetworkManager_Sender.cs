using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using Arq.Utilities;
using Arq.Collections.Concurrent;

namespace Arq.Networking
{
    public partial class NetworkManager
    {
        Arq.Collections.BitStream SerializeScripts(Connection player)
        {
            if (player._localObjects.Count == 0)
            {
                return null;
            }
            Arq.Collections.BitSerializer bs = new Arq.Collections.BitSerializer();
            var ID = player.connectionID.Value;
            bs.Serialize(ref ID, uint.MinValue, uint.MaxValue);
            lock (player._localObjects.SyncRoot)
            {
                foreach (var obj in player._localObjects)
                {
                    var scripts = obj.Value.obj.NetBehaviours;
                    uint id = obj.Key.Value;
                    bs.Serialize(ref id, uint.MinValue, uint.MaxValue);
                    for (int i = 0; i < scripts.Length; i++)
                    {
                        scripts[i].Serialize(bs);
                    }
                }
            }
            Arq.Collections.BitStream newbs = bs.GetBitStream();
            long bits = newbs.StoredBits;
            Arq.Collections.BitStream returnMe = new Arq.Collections.BitStream(bits + 128);
            returnMe.Write(Headers.Custom, minHeader, maxHeader);
            returnMe.Write(bits, 0, long.MaxValue);
            returnMe.Merge(newbs);
            return returnMe;
        }

        void SendData(Arq.Collections.BitStream data, sbyte importance = 0)
        {
            SendDataTo(data, ipEndPoint, importance);
        }

        void SendDataTo(Arq.Collections.BitStream data, IPEndPoint endPoint, sbyte importance = 0)
        {
            if (!connectedPlayers.ContainsKey1(endPoint))
            {
                Debug.LogError("IPEndPoint doesn't exist in connectedPlayers. Add the player before sending data.");
                return;
            }
            data = data.Clone();
            Connection player = connectedPlayers[endPoint];
            data.Write(PROTOCOL_ID, uint.MinValue, uint.MaxValue);
            WriteHeaders(data, importance, player.LocalPacketID, player.RemotePacketID, player.RemotePacketBitField);
            player.QueuedSendData.Enqueue(data, 0);
            player.LocalPacketID++;
            SetupPacketProtocol(player.QueuedSendData, player);
        }

#if SERVER
        void SendDataToAll(Collections.BitStream data, sbyte importance = 0)
        {
            lock (connectedPlayers.SyncRoot)
            {
                foreach (var player in connectedPlayers.Keys1)
                {
                    SendDataTo(data, player, importance);
                }
            }
        }
#endif
        void SendDataImmediate()
        {
            lock (connectedPlayers.SyncRoot)
            {
                foreach (var player in connectedPlayers.Values)
                {
                    while (player.QueuedSendData.Count > 0)
                    {
                        var packet = player.QueuedSendData.Dequeue();
                        InputCrc(packet);
                        //StartCoroutine(SendDelayed(player.connectionEndPoint, packet.GetByteArray()));
                        var bytes = packet.GetByteArray();
                        if (DataEncrypt != null && DataDecrypt != null)
                        {
                            bytes = DataEncrypt(bytes);
                        }
                        sck.SendTo(bytes, 0, bytes.Length, SocketFlags.None, player.connectionEndPoint);
                        player.ResetPacketTimer();

                        ulong bits = 1UL << 55;
                        packet.Read(out bits, ulong.MinValue, bits);
                        ushort sequence;
                        packet.Read(out sequence, ushort.MinValue, ushort.MaxValue);
                        player.StartRTTMeasure(sequence);
                    }
                }
            }
        }

        IEnumerator SendFinally()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();

                lock (connectedPlayers.SyncRoot)
                {
                    foreach (var player in connectedPlayers.Values)
                    {
                        while (player.QueuedSendData.Count > 0)
                        {
                            var packet = player.QueuedSendData.Dequeue();
                            InputCrc(packet);
                            StartCoroutine(SendDelayed(player.connectionEndPoint, packet.GetByteArray()));
                            player.ResetPacketTimer();

                            ulong bits = 1UL << 55;
                            packet.Read(out bits, ulong.MinValue, bits);
                            ushort sequence;
                            packet.Read(out sequence, ushort.MinValue, ushort.MaxValue);
                            player.StartRTTMeasure(sequence);
                        }
                    }
                }
            }
        }

        IEnumerator SendDelayed(IPEndPoint dest, byte[] data)
        {
            if (simulatedPing > 0)
            {
                yield return new WaitForSecondsRealtime(simulatedPing / 2000f - Time.unscaledDeltaTime);
            }
            if (DataEncrypt != null && DataDecrypt != null)
            {
                data = DataEncrypt(data);
            }
            sck.SendTo(data, 0, data.Length, SocketFlags.None, dest);
            //Debug.LogError("sent message");
            /*
            SerializeCompact.BitStream bs = new SerializeCompact.BitStream(data);
            bs.Read(64);
            bs.Read(64);
            bs.Read(24);
            Headers head;
            bs.Read(out head, minHeader, maxHeader);
            Debug.Log("sent data, header: " + head);*/
        }

        void SetupPacketProtocol(BlockingPriorityQueue<Arq.Collections.BitStream> packets, Connection player)
        {
            if (packets.Count < 2)
            {
                if (packets.Count == 1)
                {
                    var data = packets.Dequeue();
                    var frags = FragmentPacket(data, 512 - 7);
                    foreach (var frag in frags)
                    {
                        packets.Enqueue(frag, 0);
                    }
                }
                return;
            }
            for (int counter = packets.Count; counter > 2; counter--)
            {
                packets.Enqueue(packets.Dequeue(), 0);
            }
            var data1 = packets.Dequeue();
            var data2 = packets.Dequeue();

            sbyte importance;
            byte fragCount;
            data1.Read(out importance, sbyte.MinValue, sbyte.MaxValue);
            data1.Read(out fragCount, byte.MinValue, byte.MaxValue);
            var tempBS = data1.Clone();
            data1.Clear();
            data1.Write(importance, sbyte.MinValue, sbyte.MaxValue);
            data1.Write(fragCount, byte.MinValue, byte.MaxValue);
            data1.Merge(tempBS);

            var frags2 = FragmentPacket(data2, 512 - 7);

            if (fragCount > 1 || frags2.Length > 1 || SpecialFunctions.IntDivideRoundUp(data1.StoredBits, 8) + SpecialFunctions.IntDivideRoundUp(data2.StoredBits, 8) > 508)
            {
                packets.Enqueue(data1, 0);
                foreach (var frag in frags2)
                {
                    packets.Enqueue(frag, 0);
                }
            }
            else
            {
                CombinePackets(data1, frags2[0]);
                packets.Enqueue(data1, 0);
                player.LocalPacketID--;
            }
        }

        // add fragcount (1 byte) and frag id/packet count (1 byte)
        Arq.Collections.BitStream[] FragmentPacket(Collections.BitStream packet, uint maxBytesPerPacket)
        {
            long bytes = SpecialFunctions.IntDivideRoundUp(packet.StoredBits, 8);
            /*if (bytes < maxBytesPerPacket)
            {
                return new SerializeCompact.BitStream[] { packet };
            }*/
            sbyte importance;
            ushort sequence;
            packet.Read(out importance, sbyte.MinValue, sbyte.MaxValue);
            packet.Read(out sequence, ushort.MinValue, ushort.MaxValue);
            long fragCount = SpecialFunctions.IntDivideRoundUp(bytes, maxBytesPerPacket);
            Collections.BitStream[] packets = new Collections.BitStream[fragCount];
            Collections.BitStream data;
            byte i = 0;
            for (; i < fragCount - 1; i++)
            {
                data = new Arq.Collections.BitStream(maxBytesPerPacket * 8L);
                data.Write(importance, sbyte.MinValue, sbyte.MaxValue);
                data.Write(fragCount, byte.MinValue, byte.MaxValue);
                data.Write(i, byte.MinValue, byte.MaxValue); // fragment id
                data.Write(sequence, ushort.MinValue, ushort.MaxValue);
                data.Merge(packet, maxBytesPerPacket * 8L);
                packets[i] = data;
            }
            data = new Arq.Collections.BitStream(packet.StoredBits + 16);
            data.Write(importance, sbyte.MinValue, sbyte.MaxValue);
            data.Write(fragCount, byte.MinValue, byte.MaxValue);
            data.Write((fragCount > 1) ? i : 1L, byte.MinValue, byte.MaxValue); // fragment id or packet count
            data.Write(sequence, ushort.MinValue, ushort.MaxValue);
            data.Merge(packet);
            packets[i] = data;
            packet.Clear();
            return packets;
        }

        // merge importance, fragcount, fragid/packetcount, sequence
        // separate ack, ackbitfield, and data
        void CombinePackets(Arq.Collections.BitStream packet1, Arq.Collections.BitStream packet2)
        {
            sbyte importance;
            sbyte importance2;
            byte fragCount;
            byte packetCount;
            ushort sequence;
            packet2.Read(out importance2, sbyte.MinValue, sbyte.MaxValue);
            packet2.Read(out fragCount, byte.MinValue, byte.MaxValue);
            packet2.Read(out packetCount, byte.MinValue, byte.MaxValue);
            packet2.Read(out sequence, ushort.MinValue, ushort.MaxValue);

            packet1.Read(out importance, sbyte.MinValue, sbyte.MaxValue);
            packet1.Read(out fragCount, byte.MinValue, byte.MaxValue);
            packet1.Read(out packetCount, byte.MinValue, byte.MaxValue);
            packet1.Read(out sequence, ushort.MinValue, ushort.MaxValue);

            packetCount++;
            var tempBS = packet1.Clone();
            packet1.Clear();
            packet1.Write((importance < importance2) ? importance : importance2, sbyte.MinValue, sbyte.MaxValue);
            packet1.Write(fragCount, byte.MinValue, byte.MaxValue);
            packet1.Write(packetCount, byte.MinValue, byte.MaxValue);
            packet1.Write(sequence, ushort.MinValue, ushort.MaxValue);
            packet1.Merge(tempBS);
            packet1.Merge(packet2);
        }

        // add importance (1 byte), sequence (4 bytes), ack (4 bytes), and ackbitfield (4 bytes)
        void WriteHeaders(Arq.Collections.BitStream packet, sbyte importance, ushort sequence, ushort ack, uint ackBitField)
        {
            var newBS = packet.Clone();
            packet.Clear();
            packet.Write(importance, sbyte.MinValue, sbyte.MaxValue);
            packet.Write(sequence, ushort.MinValue, ushort.MaxValue);
            packet.Write(ack, ushort.MinValue, ushort.MaxValue);
            packet.Write(ackBitField, uint.MinValue, uint.MaxValue);
            packet.Merge(newBS);
        }

        // add crc32 (4 bytes)
        void InputCrc(Arq.Collections.BitStream packet)
        {
            uint crc = CRC32.GenerateCrc32(packet.GetByteArray());
            if (CRCEncrypt != null && CRCDecrypt != null)
            {
                crc = CRCEncrypt(crc);
            }
            Arq.Collections.BitStream temp = packet.Clone();
            packet.Clear();
            packet.Write(crc, uint.MinValue, uint.MaxValue);
            packet.Merge(temp);
        }
    }
}