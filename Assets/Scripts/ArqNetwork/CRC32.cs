namespace Arq.Networking
{
    public static class CRC32
    {
        const uint polynomial = 0xEDB88320;
        static uint[] table = new uint[256];

        static CRC32()
        {
            GenerateTable();
        }

        public static uint GenerateCrc32(byte[] data, uint previousCrc32 = 0)
        {
            uint crc = ~previousCrc32;
            for (int i = 0; i < data.Length; i++)
            {
                crc = (crc >> 8) ^ table[(crc & 0xFF) ^ data[i]];
            }
            return ~crc;
        }

        static void GenerateTable()
        {
            for (uint i = 0; i < table.Length; i++)
            {
                uint temp = i;
                for (int j = 0; j < 8; j++)
                {
                    if ((temp & 1) == 1)
                    {
                        temp = (temp >> 1) ^ polynomial;
                    }
                    else
                    {
                        temp >>= 1;
                    }
                }
                table[i] = temp;
            }

        }
    }
}