using System;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class RequireComponentOnRootExecution
{
    static RequireComponentOnRootExecution()
    {
        EditorApplication.delayCall += Update;
        EditorApplication.hierarchyWindowChanged += Update;
        EditorApplication.projectWindowChanged += Update;
    }

    static void Update()
    {
        foreach (var comp in UnityEngine.Object.FindObjectsOfType<Component>())
        {
            RequireComponentOnRootAttribute attribute = (RequireComponentOnRootAttribute) Attribute.GetCustomAttribute(comp.GetType(), typeof(RequireComponentOnRootAttribute));
            if (attribute != null)
            {
                Transform root = comp.transform.root;

                if (root.GetComponent(attribute.type) == null)
                {
                    Debug.LogWarning(comp.GetType() + " requires the root gameobject to contain " + attribute.type + ". Added to '" + root.name + "'...");
                    root.gameObject.AddComponent(attribute.type);
                }
                if (attribute.singleton)
                {
                    var arr = root.GetComponentsInChildren(attribute.type);
                    for (int i = 1; i < arr.Length; i++)
                    {
                        Debug.LogWarning(comp.GetType() + " only allows one " + attribute.type + " in its transform hierarchy. Removed " + attribute.type + " from '" + arr[i].name + "'...");
                        UnityEngine.Object.DestroyImmediate(arr[i]);
                    }
                }
            }
        }
    }
}