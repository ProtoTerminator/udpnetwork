public struct CircularInt16
{
    short value;

    public short ToInt16()
    {
        return value;
    }

    public ushort ToUInt16()
    {
        return (ushort)value;
    }

    public CircularInt16(short value)
    {
        this.value = value;
    }

    public CircularInt16(ushort value)
        : this((short)value)
    {

    }

    public static bool operator ==(CircularInt16 i1, CircularInt16 i2)
    {
        return i1.value == i2.value;
    }

    public static bool operator !=(CircularInt16 i1, CircularInt16 i2)
    {
        return !(i1 == i2);
    }

    public static bool operator >(CircularInt16 i1, CircularInt16 i2)
    {
        return
            (i1.value > i2.value) &&
            (i2.value - i1.value < 0)
               ||
            (i1.value < i2.value) &&
            (i2.value - i1.value > 0);
    }

    public static bool operator <(CircularInt16 i1, CircularInt16 i2)
    {
        return i1 != i2 && !(i1 > i2);
    }

    public static bool operator >=(CircularInt16 i1, CircularInt16 i2)
    {
        return i1 == i2 || i1 > i2;
    }

    public static bool operator <=(CircularInt16 i1, CircularInt16 i2)
    {
        return !(i1 > i2);
    }

    public static CircularInt16 operator ++(CircularInt16 i1)
    {
        return i1 + (short) 1;
    }

    public static CircularInt16 operator --(CircularInt16 i1)
    {
        return i1 - (short) 1;
    }

    public static CircularInt16 operator +(CircularInt16 i1, CircularInt16 i2)
    {
        unchecked
        {
            return new CircularInt16((short)(i1.value + i2.value));
        }
    }

    public static CircularInt16 operator -(CircularInt16 i1, CircularInt16 i2)
    {
        unchecked
        {
            return new CircularInt16((short)(i1.value - i2.value));
        }
    }

    public static CircularInt16 operator *(CircularInt16 i1, CircularInt16 i2)
    {
        unchecked
        {
            return new CircularInt16((short)(i1.value * i2.value));
        }
    }

    public static CircularInt16 operator /(CircularInt16 i1, CircularInt16 i2)
    {
        unchecked
        {
            return new CircularInt16((short)(i1.value / i2.value));
        }
    }

    public static explicit operator short(CircularInt16 i1)
    {
        return i1.value;
    }

    public static implicit operator CircularInt16(short i1)
    {
        return new CircularInt16(i1);
    }

    public static explicit operator ushort(CircularInt16 i1)
    {
        return i1.ToUInt16();
    }

    public static implicit operator CircularInt16(ushort i1)
    {
        return new CircularInt16(i1);
    }

    public override bool Equals(object obj)
    {
        return obj != null &&
                obj.GetType() == typeof(CircularInt16) &&
                this == (CircularInt16)obj;
    }

    public override int GetHashCode()
    {
        return value.GetHashCode();
    }

    public override string ToString()
    {
        return "Circular Int: " + value + ", UInt: " + ToUInt16();
    }
}