using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
public class RequireComponentOnRootAttribute : Attribute
{
    /// <summary>
    /// The type of component to be required on the root gameobject.
    /// </summary>
    public Type type;
    /// <summary>
    /// Whether or not only one instance of the required component is allowed in the entire transform hierarchy.
    /// </summary>
    public bool singleton = false;

    public RequireComponentOnRootAttribute(Type type)
    {
        if (!(type.IsSubclassOf(typeof(Component)) || type == typeof(Component)))
        {
            throw new ArgumentException("type must derive from Component or MonoBehaviour.", "type");
        }
        this.type = type;
    }
}