public struct CircularInt32
{
    int value;

    public int ToInt32()
    {
        return value;
    }

    public uint ToUInt32()
    {
        return (uint) value;
    }

    public CircularInt32(int value)
    {
        this.value = value;
    }

    public CircularInt32(uint value)
        : this((int) value)
    {

    }

    public static bool operator ==(CircularInt32 i1, CircularInt32 i2)
    {
        return i1.value == i2.value;
    }

    public static bool operator !=(CircularInt32 i1, CircularInt32 i2)
    {
        return !(i1 == i2);
    }

    public static bool operator >(CircularInt32 i1, CircularInt32 i2)
    {
        return
            (i1.value > i2.value) &&
            (i2.value - i1.value < 0)
               ||
            (i1.value < i2.value) &&
            (i2.value - i1.value > 0);
    }

    public static bool operator <(CircularInt32 i1, CircularInt32 i2)
    {
        return i1 != i2 && !(i1 > i2);
    }

    public static bool operator >=(CircularInt32 i1, CircularInt32 i2)
    {
        return i1 == i2 || i1 > i2;
    }

    public static bool operator <=(CircularInt32 i1, CircularInt32 i2)
    {
        return !(i1 > i2);
    }

    public static CircularInt32 operator ++(CircularInt32 i1)
    {
        return i1 + 1;
    }

    public static CircularInt32 operator --(CircularInt32 i1)
    {
        return i1 - 1;
    }

    public static CircularInt32 operator +(CircularInt32 i1, CircularInt32 i2)
    {
        unchecked
        {
            return new CircularInt32(i1.value + i2.value);
        }
    }

    public static CircularInt32 operator -(CircularInt32 i1, CircularInt32 i2)
    {
        unchecked
        {
            return new CircularInt32(i1.value - i2.value);
        }
    }

    public static CircularInt32 operator *(CircularInt32 i1, CircularInt32 i2)
    {
        unchecked
        {
            return new CircularInt32(i1.value * i2.value);
        }
    }

    public static CircularInt32 operator /(CircularInt32 i1, CircularInt32 i2)
    {
        unchecked
        {
            return new CircularInt32(i1.value / i2.value);
        }
    }

    public static explicit operator int(CircularInt32 i1)
    {
        return i1.value;
    }

    public static implicit operator CircularInt32(int i1)
    {
        return new CircularInt32(i1);
    }

    public static explicit operator uint(CircularInt32 i1)
    {
        return i1.ToUInt32();
    }

    public static implicit operator CircularInt32(uint i1)
    {
        return new CircularInt32(i1);
    }

    public override bool Equals(object obj)
    {
        return obj != null &&
                obj.GetType() == typeof(CircularInt32) &&
                this == (CircularInt32) obj;
    }

    public override int GetHashCode()
    {
        return value.GetHashCode();
    }

    public override string ToString()
    {
        return "Circular Int: " + value + ", UInt: " + ToUInt32();
    }
}