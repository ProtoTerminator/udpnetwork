namespace Arq.Collections
{
	public interface ISerializable
	{
		/// <summary>
		/// Serializes data to the bit serializer or deserializes data from it
		/// returns true if serialization was successful, false otherwise
		/// </summary>
		/// <param name="bitSerializer">bit serializer for data to be serialized to or deserialized from</param>
		/// <returns>true if serialization was successful, false otherwise</returns>
		bool Serialize(BitSerializer bitSerializer);
	}
}
