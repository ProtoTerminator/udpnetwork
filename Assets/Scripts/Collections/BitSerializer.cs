using System;
using System.Globalization;
using UnityEngine;

namespace Arq.Collections
{
	/// <summary>
	/// Used to serialize and deserialize data utilizing the BitStream class.
	/// </summary>
	public class BitSerializer
	{
		BitStream bs = new BitStream();
		bool isReading;

		public bool IsReading
		{
			get { return isReading; }
			private set { isReading = value; }
		}
		public bool IsWriting
		{
			get { return !isReading; }
			private set { isReading = !value; }
		}

		/// <summary>
		/// Create a new BitSerializer
		/// </summary>
		/// <param name="read">Writing or reading. Set to true to read, false to write.</param>
		public BitSerializer(bool read = false)
		{
			isReading = read;
		}

		/// <summary>
		/// Create a new BitSerializer
		/// </summary>
		/// <param name="bits">How many bits you expect this serializer will hold. A closer value nets increased performance.</param>
		/// <param name="read">Writing or reading. Set to true to read, false to write.</param>
		public BitSerializer(long bits, bool read = false)
			: this(read)
		{
			bs = new BitStream(bits);
		}

		/// <summary>
		/// Create a new BitSerializer
		/// </summary>
		/// <param name="bitStream">The BitStream to read from or write to. Changes made to this bitserializer affect the bitstream. Input bitStream.Clone() if this behavior is unwanted.</param>
		/// <param name="read">Writing or reading. Set to true to read, false to write.</param>
		public BitSerializer(BitStream bitStream, bool read = true)
			: this(read)
		{
			bs = bitStream;
		}

		/// <summary>
		/// Create a new BitSerializer
		/// </summary>
		/// <param name="data">The data to be read from</param>
		/// <param name="read">Writing or reading. Set to true to read, false to write.</param>
		public BitSerializer(byte[] data, bool read = true)
			: this(read)
		{
			bs = new BitStream(data);
		}

		/// <summary>
		/// Returns the bitstream used to store the data. Changes made to the bitstream affect this bitserializer. Clone the bitstream before making changes if this behavior is unwanted.
		/// </summary>
		/// <returns>the bit stream used to store the data</returns>
		public BitStream GetBitStream()
		{
			return bs;
		}

		/// <summary>
		/// Get the bits stored in a byte array (left-endian)
		/// </summary>
		/// <returns>byte array of bits</returns>
		public byte[] GetByteArray()
		{
			return bs.GetByteArray();
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref ulong data, ulong min, ulong max)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max);
			}
			bs.Write(data, min, max);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref uint data, uint min, uint max)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max);
			}
			bs.Write(data, min, max);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref ushort data, ushort min, ushort max)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max);
			}
			bs.Write(data, min, max);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref byte data, byte min, byte max)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max);
			}
			bs.Write(data, min, max);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to '\0'.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref char data, char min, char max)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max);
			}
			bs.Write(data, min, max);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref long data, long min, long max)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max);
			}
			bs.Write(data, min, max);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref int data, int min, int max)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max);
			}
			bs.Write(data, min, max);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref short data, short min, short max)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max);
			}
			bs.Write(data, min, max);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref sbyte data, sbyte min, sbyte max)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max);
			}
			bs.Write(data, min, max);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to false.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref bool data)
		{
			if (IsReading)
			{
				return bs.Read(out data);
			}
			bs.Write(data);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <param name="precision">how many digits after the decimal</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref float data, float min, float max, byte precision)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max, precision);
			}
			bs.Write(data, min, max, precision);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <param name="precision">how many digits after the decimal</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref double data, double min, double max, byte precision)
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max, precision);
			}
			bs.Write(data, min, max, precision);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible number to be serialized</param>
		/// <param name="max">The largest possible number to be serialized</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize<TEnum>(ref TEnum data, TEnum min, TEnum max) where TEnum : struct, IComparable, IFormattable, IConvertible
		{
			if (IsReading)
			{
				return bs.Read(out data, min, max);
			}
			bs.Write(data, min, max);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible vector to be serialized</param>
		/// <param name="max">The largest possible vector to be serialized</param>
		/// <param name="precision">how many digits after the decimal in all dimensions</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref Vector2 data, Vector2 min, Vector2 max, byte precision)
		{
			if (IsReading)
			{
                Vector2 newData = data;
				if (!bs.Read(out newData.x, min.x, max.x, precision) || 
                    !bs.Read(out newData.y, min.y, max.y, precision))
                {
                    data = Vector2.zero;
					return false;
                }
                data = newData;
                return true;
            }
			bs.Write(data.x, min.x, max.x, precision);
			bs.Write(data.y, min.y, max.y, precision);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible vector to be serialized</param>
		/// <param name="max">The largest possible vector to be serialized</param>
		/// <param name="precision">how many digits after the decimal in all dimensions</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref Vector3 data, Vector3 min, Vector3 max, byte precision)
		{
			if (IsReading)
            {
                Vector3 newData = data;
                if (!bs.Read(out newData.x, min.x, max.x, precision) ||
                    !bs.Read(out newData.y, min.y, max.y, precision) ||
                    !bs.Read(out newData.z, min.z, max.z, precision))
                {
                    data = Vector3.zero;
                    return false;
                }
                data = newData;
                return true;
            }
			bs.Write(data.x, min.x, max.x, precision);
			bs.Write(data.y, min.y, max.y, precision);
			bs.Write(data.z, min.z, max.z, precision);
			return true;
		}

		/// <summary>
		/// Serialize or Deserialize the data
		/// If data was read or written properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">Data to be serialized or deserialized</param>
		/// <param name="min">The smallest possible vector to be serialized</param>
		/// <param name="max">The largest possible vector to be serialized</param>
		/// <param name="precision">how many digits after the decimal in all dimensions</param>
		/// <returns>true if data was read or written properly, false otherwise</returns>
		public bool Serialize(ref Vector4 data, Vector4 min, Vector4 max, byte precision)
		{
			if (IsReading)
            {
                Vector4 newData = data;
                if (!bs.Read(out newData.x, min.x, max.x, precision) ||
                    !bs.Read(out newData.y, min.y, max.y, precision) ||
                    !bs.Read(out newData.z, min.z, max.z, precision) ||
                    !bs.Read(out newData.w, min.w, max.w, precision))
                {
                    data = Vector4.zero;
                    return false;
                }
                data = newData;
                return true;
            }
			bs.Write(data.x, min.x, max.x, precision);
			bs.Write(data.y, min.y, max.y, precision);
			bs.Write(data.z, min.z, max.z, precision);
			bs.Write(data.w, min.w, max.w, precision);
			return true;
		}
	}
}