using System;
using System.Collections;
using System.Collections.Generic;

namespace Arq.Collections
{
    public class TwoKeyOrDictionary<TKey1, TKey2, TValue> : ICollection<KeyValueTrio<TKey1, TKey2, TValue>>
    {
        readonly Dictionary<TKey1, TValue> baseDictionary = new Dictionary<TKey1, TValue>();
        readonly Dictionary<TKey2, TKey1> subDictionary = new Dictionary<TKey2, TKey1>();
        readonly Dictionary<TKey1, TKey2> primaryToSubkeyMapping = new Dictionary<TKey1, TKey2>();

        public TValue this[TKey1 key]
        {
            get
            {
                return baseDictionary[key];
            }

            set
            {
                baseDictionary[key] = value;
            }
        }

        public TValue this[TKey2 key]
        {
            get
            {
                return baseDictionary[subDictionary[key]];
            }

            set
            {
                baseDictionary[subDictionary[key]] = value;
            }
        }

        public void SetKey1(TKey1 key, TValue value)
        {
            baseDictionary[key] = value;
        }

        public void SetKey2(TKey2 key, TValue value)
        {
            baseDictionary[subDictionary[key]] = value;
        }

        public TValue GetKey1(TKey1 key)
        {
            return baseDictionary[key];
        }

        public TValue GetKey2(TKey2 key)
        {
            return baseDictionary[subDictionary[key]];
        }

        public int Count
        {
            get
            {
                return baseDictionary.Count;
            }
        }

        public ICollection<TKey1> Keys1
        {
            get
            {
                return baseDictionary.Keys;
            }
        }

        public ICollection<TKey2> Keys2
        {
            get
            {
                return subDictionary.Keys;
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                return baseDictionary.Values;
            }
        }

        bool ICollection<KeyValueTrio<TKey1, TKey2, TValue>>.IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public void Add(KeyValueTrio<TKey1, TKey2, TValue> item)
        {
            Add(item.Key1, item.Key2, item.Value);
        }

        public void Add(TKey1 key1, TKey2 key2, TValue value)
        {
            if (key1 == null || key2 == null)
            {
                throw new ArgumentNullException("keys cannot be null");
            }
            if (baseDictionary.ContainsKey(key1))
            {
                throw new ArgumentException("key1 already exists");
            }
            if (subDictionary.ContainsKey(key2))
            {
                throw new ArgumentException("key2 already exists");
            }
            baseDictionary.Add(key1, value);
            subDictionary.Add(key2, key1);
            primaryToSubkeyMapping.Add(key1, key2);
        }

        public void Clear()
        {
            baseDictionary.Clear();
            subDictionary.Clear();
            primaryToSubkeyMapping.Clear();
        }

        public bool ContainsKey1(TKey1 key)
        {
            return baseDictionary.ContainsKey(key);
        }

        public bool ContainsKey2(TKey2 key)
        {
            return subDictionary.ContainsKey(key);
        }

        public bool ContainsValue(TValue value)
        {
            return baseDictionary.ContainsValue(value);
        }

        public bool RemoveKey1(TKey1 key)
        {
            if (key == null)
            {
                throw new ArgumentNullException();
            }
            if (!baseDictionary.ContainsKey(key))
            {
                return false;
            }
            baseDictionary.Remove(key);
            subDictionary.Remove(primaryToSubkeyMapping[key]);
            primaryToSubkeyMapping.Remove(key);
            return true;
        }

        public bool RemoveKey2(TKey2 key)
        {
            if (key == null)
            {
                throw new ArgumentNullException();
            }
            if (!subDictionary.ContainsKey(key))
            {
                return false;
            }
            baseDictionary.Remove(subDictionary[key]);
            primaryToSubkeyMapping.Remove(subDictionary[key]);
            subDictionary.Remove(key);
            return true;
        }

        public IEnumerator<KeyValueTrio<TKey1, TKey2, TValue>> GetEnumerator()
        {
            if (Count == 0)
            {
                yield break;
            }
            foreach (var item in baseDictionary)
            {
                yield return new KeyValueTrio<TKey1, TKey2, TValue>(item.Key, primaryToSubkeyMapping[item.Key], item.Value);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator<KeyValueTrio<TKey1, TKey2, TValue>> IEnumerable<KeyValueTrio<TKey1, TKey2, TValue>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool Contains(KeyValueTrio<TKey1, TKey2, TValue> item)
        {
            return baseDictionary.ContainsKey(item.Key1) && subDictionary.ContainsKey(item.Key2) && baseDictionary.ContainsValue(item.Value);
        }

        public void CopyTo(KeyValueTrio<TKey1, TKey2, TValue>[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException("The array cannot be null.");
            }
            if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException("The starting array index cannot be negative.");
            }
            if (Count > array.Length - arrayIndex + 1)
            {
                throw new ArgumentException("The destination array has fewer elements than the collection.");
            }

            int i = 0;
            foreach (var item in this)
            {
                array[i + arrayIndex] = item;
                i++;
            }
        }

        public bool Remove(KeyValueTrio<TKey1, TKey2, TValue> item)
        {
            return RemoveKey1(item.Key1);
        }
    }
}