using System;

namespace Arq.Collections
{
    /// <summary>
    /// Priority queue removes added items highest priority items first, ties broken by First-In-First-Out.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PriorityQueue<T>
    {

        struct Node
        {
            public T item;
            public int priority;
            public CircularInt32 insertionIndex;
        }

        Node[] items;
        bool _resizeable;
        CircularInt32 _numItemsEverEnqueued = 0;

        /// <summary>
        /// How many items are currently in the queue
        /// </summary>
        public int Count
        {
            get;
            private set;
        }

        /// <summary>
        /// How many items the queue can hold. 0 == infinite.
        /// </summary>
        public int Capacity
        {
            get
            {
                return _resizeable ? 0 : items.Length;
            }
        }

        /// <summary>
        /// Create a new resizeable priority queue with default capacity (8)
        /// </summary>
        public PriorityQueue() : this(8) { }

        /// <summary>
        /// Create a new priority queue
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="resizeable"></param>
        public PriorityQueue(int capacity, bool resizeable = true)
        {
            if (capacity < 2)
            {
                throw new ArgumentException("New queue size cannot be smaller than 2", "capacity");
            }
            items = new Node[capacity];
            Count = 0;
            _resizeable = resizeable;
        }

        /// <summary>
        /// Add an object to the queue. If queue is full and resizeable is true, increases the capacity. If queue is full and resizeable is false, does nothing, returns false.
        /// </summary>
        /// <param name="item">object to add to queue</param>
        /// <param name="priority">object's priority, lower # = higher priority, ties are broken by FIFO</param>
        /// <returns>true if added successfully, false otherwise (queue is full)</returns>
        public bool Enqueue(T item, int priority)
        {
            if (Count == items.Length)
            {
                if (_resizeable)
                {
                    Array.Resize(ref items, items.Length * 3 / 2 + 1);
                }
                else
                {
                    return false;
                }
            }
            items[Count] = new Node() { item = item, priority = priority, insertionIndex = _numItemsEverEnqueued++ };
            percolateUp(Count);
            Count++;
            return true;
        }

        void percolateUp(int index)
        {
            while (true)
            {
                if (index == 0)
                {
                    break;
                }

                int parent = (index % 2 == 0) ? index / 2 - 1 : index / 2;

                if (HasHigherPriority(items[parent], items[index]))
                {
                    var temp = items[index];
                    items[index] = items[parent];
                    items[parent] = temp;
                    index = parent;
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Removes and returns the highest priority object in the queue. Ties are broken by FIFO.
        /// Returns an object's default value if the queue is empty.
        /// </summary>
        /// <returns></returns>
        public T Dequeue()
        {
            if (Count == 0)
            {
                return default(T);
            }

            var item = items[0].item;
            Count--;
            items[0] = items[Count];
            items[Count] = new Node();
            percolateDown(0);
            return item;
        }

        void percolateDown(int index)
        {
            while (true)
            {
                int child = index * 2 + 1;
                if (child >= Count)
                {
                    break;
                }
                if (child + 1 < Count && HasHigherPriority(items[child + 1], items[child]))
                {
                    ++child;
                }
                if (HasHigherPriority(items[child], items[index]))
                {
                    var temp = items[index];
                    items[index] = items[child];
                    items[child] = temp;
                }
                else
                {
                    break;
                }
                index = child;
            }

        }

        bool HasHigherPriority(Node higher, Node lower)
        {
            return (higher.priority < lower.priority || (higher.priority == lower.priority && higher.insertionIndex < lower.insertionIndex));
        }
    }
}