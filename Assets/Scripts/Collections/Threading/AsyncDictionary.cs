using System;
using System.Collections;
using System.Collections.Generic;

namespace Arq.Collections.Concurrent
{
    public class AsyncDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        object baton = new object();
        Dictionary<TKey, TValue> dict;

        public object SyncRoot
        {
            get
            {
                return baton;
            }
        }

        public AsyncDictionary()
        {
            dict = new Dictionary<TKey, TValue>();
        }
        public AsyncDictionary(IEqualityComparer<TKey> comparer)
        {
            dict = new Dictionary<TKey, TValue>(comparer);
        }
        public AsyncDictionary(IDictionary<TKey, TValue> dictionary)
        {
            dict = new Dictionary<TKey, TValue>(dictionary);
        }
        public AsyncDictionary(int capacity)
        {
            dict = new Dictionary<TKey, TValue>(capacity);
        }
        public AsyncDictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
        {
            dict = new Dictionary<TKey, TValue>(dictionary, comparer);
        }
        public AsyncDictionary(int capacity, IEqualityComparer<TKey> comparer)
        {
            dict = new Dictionary<TKey, TValue>(capacity, comparer);
        }

        public TValue this[TKey key]
        {
            get
            {
                lock (baton)
                {
                    return dict[key];
                }
            }

            set
            {
                lock (baton)
                {
                    dict[key] = value;
                }
            }
        }

        public int Count
        {
            get
            {
                lock (baton)
                {
                    return dict.Count;
                }
            }
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public ICollection<TKey> Keys
        {
            get
            {
                lock (baton)
                {
                    return dict.Keys;
                }
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                lock (baton)
                {
                    return dict.Values;
                }
            }
        }

        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
        {
            throw new NotImplementedException();
        }

        public void Add(TKey key, TValue value)
        {
            lock (baton)
            {
                dict.Add(key, value);
            }
        }

        public void Clear()
        {
            lock (baton)
            {
                dict.Clear();
            }
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
        {
            lock (baton)
            {
                return ((ICollection<KeyValuePair<TKey, TValue>>)dict).Contains(item);
            }
        }

        public bool ContainsKey(TKey key)
        {
            lock (baton)
            {
                return dict.ContainsKey(key);
            }
        }

        void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            lock (baton)
            {
                ((ICollection<KeyValuePair<TKey, TValue>>)dict).CopyTo(array, arrayIndex);
            }
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            lock (baton)
            {
                return dict.GetEnumerator();
            }
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(TKey key)
        {
            lock (baton)
            {
                return dict.Remove(key);
            }
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            lock (baton)
            {
                return dict.TryGetValue(key, out value);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}