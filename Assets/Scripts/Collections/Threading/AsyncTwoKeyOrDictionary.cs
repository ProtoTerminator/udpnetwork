using System;
using System.Collections;
using System.Collections.Generic;

namespace Arq.Collections.Concurrent
{
    public class AsyncTwoKeyOrDictionary<TKey1, TKey2, TValue> : ICollection<KeyValueTrio<TKey1, TKey2, TValue>>
    {
        object baton = new object();
        readonly TwoKeyOrDictionary<TKey1, TKey2, TValue> baseDictionary = new TwoKeyOrDictionary<TKey1, TKey2, TValue>();

        public object SyncRoot
        {
            get
            {
                return baton;
            }
        }

        public TValue this[TKey1 key]
        {
            get
            {
                lock (baton)
                {
                    return baseDictionary[key];
                }
            }

            set
            {
                lock (baton)
                {
                    baseDictionary[key] = value;
                }
            }
        }

        public TValue this[TKey2 key]
        {
            get
            {
                lock (baton)
                {
                    return baseDictionary[key];
                }
            }

            set
            {
                lock (baton)
                {
                    baseDictionary[key] = value;
                }
            }
        }

        public void SetKey1(TKey1 key, TValue value)
        {
            lock (baton)
            {
                baseDictionary[key] = value;
            }
        }

        public void SetKey2(TKey2 key, TValue value)
        {
            lock (baton)
            {
                baseDictionary[key] = value;
            }
        }

        public TValue GetKey1(TKey1 key)
        {
            lock (baton)
            {
                return baseDictionary[key];
            }
        }

        public TValue GetKey2(TKey2 key)
        {
            lock (baton)
            {
                return baseDictionary[key];
            }
        }

        public int Count
        {
            get
            {
                lock (baton)
                {
                    return baseDictionary.Count;
                }
            }
        }

        public ICollection<TKey1> Keys1
        {
            get
            {
                lock (baton)
                {
                    return baseDictionary.Keys1;
                }
            }
        }

        public ICollection<TKey2> Keys2
        {
            get
            {
                lock (baton)
                {
                    return baseDictionary.Keys2;
                }
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                lock (baton)
                {
                    return baseDictionary.Values;
                }
            }
        }

        bool ICollection<KeyValueTrio<TKey1, TKey2, TValue>>.IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public void Add(KeyValueTrio<TKey1, TKey2, TValue> item)
        {
            Add(item.Key1, item.Key2, item.Value);
        }

        public void Add(TKey1 key1, TKey2 key2, TValue value)
        {
            lock (baton)
            {
                baseDictionary.Add(key1, key2, value);
            }
        }

        public void Clear()
        {
            lock (baton)
            {
                baseDictionary.Clear();
            }
        }

        public bool ContainsKey1(TKey1 key)
        {
            lock (baton)
            {
                return baseDictionary.ContainsKey1(key);
            }
        }

        public bool ContainsKey2(TKey2 key)
        {
            lock (baton)
            {
                return baseDictionary.ContainsKey2(key);
            }
        }

        public bool ContainsValue(TValue value)
        {
            lock (baton)
            {
                return baseDictionary.ContainsValue(value);
            }
        }

        public bool RemoveKey1(TKey1 key)
        {
            lock (baton)
            {
                return baseDictionary.RemoveKey1(key);
            }
        }

        public bool RemoveKey2(TKey2 key)
        {
            lock (baton)
            {
                return baseDictionary.RemoveKey2(key);
            }
        }

        public IEnumerator<KeyValueTrio<TKey1, TKey2, TValue>> GetEnumerator()
        {
            lock (baton)
            {
                return baseDictionary.GetEnumerator();
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            lock (baton)
            {
                return baseDictionary.GetEnumerator();
            }
        }

        IEnumerator<KeyValueTrio<TKey1, TKey2, TValue>> IEnumerable<KeyValueTrio<TKey1, TKey2, TValue>>.GetEnumerator()
        {
            lock (baton)
            {
                return baseDictionary.GetEnumerator();
            }
        }

        public bool Contains(KeyValueTrio<TKey1, TKey2, TValue> item)
        {
            lock (baton)
            {
                return baseDictionary.Contains(item);
            }
        }

        public void CopyTo(KeyValueTrio<TKey1, TKey2, TValue>[] array, int arrayIndex)
        {
            lock (baton)
            {
                baseDictionary.CopyTo(array, arrayIndex);
            }
        }

        public bool Remove(KeyValueTrio<TKey1, TKey2, TValue> item)
        {
            lock (baton)
            {
                return RemoveKey1(item.Key1);
            }
        }
    }
}