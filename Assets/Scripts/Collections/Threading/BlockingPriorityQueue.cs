using System.Threading;

namespace Arq.Collections.Concurrent
{
    /// <summary>
    /// A thread-safe priority queue.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BlockingPriorityQueue<T>
    {
        object baton = new object();
        PriorityQueue<T> pq;

        public object SyncRoot
        {
            get
            {
                return baton;
            }
        }

        /// <summary>
        /// How many items are currently in the queue
        /// </summary>
        public int Count
        {
            get
            {
                lock (baton)
                {
                    return pq.Count;
                }
            }
        }

        /// <summary>
        /// How many items the queue can hold. 0 == infinite.
        /// </summary>
        public int Capacity
        {
            get
            {
                return pq.Capacity;
            }
        }

        /// <summary>
        /// Create a new resizeable async priority queue with default capacity (8)
        /// </summary>
        public BlockingPriorityQueue()
        {
            pq = new PriorityQueue<T>();
        }

        /// <summary>
        /// Create a new priority queue
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="resizeable"></param>
        public BlockingPriorityQueue(int capacity, bool resizeable = true)
        {
            pq = new PriorityQueue<T>(capacity, resizeable);
        }

        /// <summary>
        /// Add an object to the queue. If queue is full and resizeable is true, increases the capacity. If queue is full and resizeable is false, does nothing, returns false.
        /// </summary>
        /// <param name="item">object to add to queue</param>
        /// <param name="priority">object's priority, lower # = higher priority, ties are broken by FIFO</param>
        /// <returns>true if added successfully, false otherwise (queue is full)</returns>
        public bool Enqueue(T item, int priority)
        {
            lock (baton)
            {
                bool added = pq.Enqueue(item, priority);
                if (pq.Count == 1)
                {
                    Monitor.Pulse(baton);
                }
                return added;
            }
        }

        /// <summary>
        /// Removes and returns the highest priority object in the queue. Ties are broken by FIFO.
        /// WARNING: if the queue is empty when this is called, the thread WILL BLOCK until a new item is added to the queue in another thread. If this behaviour is not wanted, be sure to check Count > 0.
        /// </summary>
        /// <returns></returns>
        public T Dequeue()
        {
            lock (baton)
            {
                while (pq.Count == 0)
                {
                    Monitor.Wait(baton);
                }
                return pq.Dequeue();
            }
        }
    }
}