using System.Collections;
using System.Collections.Generic;

namespace Arq.Collections.Concurrent
{
    public class NonBlockingQueue<T> : IEnumerable<T>
    {
        readonly Queue<T> queue1;
        readonly Queue<T> queue2;
        volatile bool read1 = true;
        volatile bool isWriting = false;
        object syncRead = new object();

        public object SyncRead
        {
            get
            {
                return syncRead;
            }
        }

        public NonBlockingQueue()
        {
            queue1 = new Queue<T>();
            queue2 = new Queue<T>();
        }
        public NonBlockingQueue(int count)
        {
            queue1 = new Queue<T>(count);
            queue2 = new Queue<T>(count);
        }
        public NonBlockingQueue(IEnumerable<T> collection)
        {
            queue1 = new Queue<T>(collection);
            queue2 = new Queue<T>();
        }

        public int Count
        {
            get
            {
                lock (syncRead)
                {
                    bool left = read1;
                    int count = (left) ? queue1.Count : queue2.Count;
                    while (isWriting) { }
                    isWriting = true;
                    count += (left) ? queue2.Count : queue1.Count;
                    isWriting = false;
                    return count;
                }
            }
        }

        public void Clear()
        {
            lock (syncRead)
            {
                if (read1)
                {
                    queue1.Clear();
                }
                else
                {
                    queue2.Clear();
                }
                while (isWriting) { }
                isWriting = true;
                if (read1)
                {
                    queue2.Clear();
                }
                else
                {
                    queue1.Clear();
                }
                isWriting = false;
            }
        }
        public bool Contains(T item)
        {
            lock (syncRead)
            {
                bool left = read1;
                if (left)
                {
                    if (queue1.Contains(item))
                    {
                        return true;
                    }
                }
                else
                {
                    if (queue2.Contains(item))
                    {
                        return true;
                    }
                }
                while (isWriting) { }
                isWriting = true;
                if (left)
                {
                    if (queue2.Contains(item))
                    {
                        isWriting = false;
                        return true;
                    }
                }
                else
                {
                    if (queue1.Contains(item))
                    {
                        isWriting = false;
                        return true;
                    }
                }
                isWriting = false;
                return false;
            }
        }
        public void CopyTo(T[] array, int idx)
        {
            lock (syncRead)
            {
                bool left = read1;
                int index;
                if (left)
                {
                    index = queue1.Count;
                    queue1.CopyTo(array, idx);
                }
                else
                {
                    index = queue2.Count;
                    queue2.CopyTo(array, idx);
                }
                while (isWriting) { }
                isWriting = true;
                if (left)
                {
                    queue2.CopyTo(array, idx + index);
                }
                else
                {
                    queue1.CopyTo(array, idx + index);
                }
                isWriting = false;
            }
        }
        public T Dequeue()
        {
            lock (syncRead)
            {
                var readQueue = (read1) ? queue1 : queue2;
                if (readQueue.Count > 0)
                {
                    return readQueue.Dequeue();
                }
                read1 = !read1;
                while (isWriting) { }
                readQueue = (read1) ? queue1 : queue2;
                return readQueue.Dequeue();
            }
        }
        public void Enqueue(T item)
        {
            while (isWriting) { }
            isWriting = true;
            if (read1)
                queue1.Enqueue(item);
            else
                queue2.Enqueue(item);
            isWriting = false;
        }
        public T Peek()
        {
            lock (syncRead)
            {
                var readQueue = (read1) ? queue1 : queue2;
                if (readQueue.Count > 0)
                {
                    return readQueue.Peek();
                }
                read1 = !read1;
                while (isWriting) { }
                readQueue = (read1) ? queue1 : queue2;
                return readQueue.Peek();
            }
        }
        public T[] ToArray()
        {
            T[] arr;
            lock (syncRead)
            {
                while (isWriting) { }
                isWriting = true;
                arr = new T[queue1.Count + queue2.Count];
                if (read1)
                {
                    queue1.CopyTo(arr, 0);
                    queue2.CopyTo(arr, queue1.Count);
                }
                else
                {
                    queue2.CopyTo(arr, 0);
                    queue1.CopyTo(arr, queue2.Count);
                }
                isWriting = false;
            }
            return arr;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((ICollection<T>)ToArray()).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}