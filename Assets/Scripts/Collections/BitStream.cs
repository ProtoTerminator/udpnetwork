using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace Arq.Collections
{
	/// <summary>
	/// Used to store data as bits. Acts as a queue - first data added is the first data removed.
	/// Data should be read in the same order it is written. If read in a different order, it gives undefined results.
	/// Reading from an empty BitStream returns 0.
	/// </summary>
	public class BitStream
	{
		ulong scratchWrite = 0;
		int scratchWriteBits = 0;
		ulong scratchRead = 0;
		int scratchReadBits = 0;
		Queue<ulong> buffer = new Queue<ulong>();

		/// <summary>
		/// How many bits are currently in the BitStream
		/// </summary>
		public long StoredBits
		{
			get;
			private set;
		}

		/// <summary>
		/// Make a new BitStream
		/// </summary>
		public BitStream() { }

		/// <summary>
		/// Make a new BitStream
		/// </summary>
		/// <param name="bitCount">How many bits you expect this stream will hold. A closer value nets increased performance.</param>
		public BitStream(long bitCount)
		{
            if (bitCount < 0)
            {
                throw new ArgumentException("bitCount cannot be less than zero");
            }
			buffer = new Queue<ulong>((int) IntDivideRoundUp(bitCount, 64));
		}

        /// <summary>
        /// Make a new BitStream containing bits from the byte array
        /// NOTE: StoredBits may return a higher count than there are actual bits to read if the byte array came from another BitStream.
        /// </summary>
        /// <param name="bits">contains bits to be stored in the bitstream</param>
        public BitStream(byte[] bits)
            : this(bits.Length * 8)
        {
            foreach (var bite in bits)
            {
                Write(bite, 8);
            }
        }

        /// <summary>
        /// Get the bits stored in a ulong array (earlier added elements are at lower indexes)
        /// </summary>
        /// <returns>ulong array of bits</returns>
        public ulong[] GetUlongArray()
        {
            if (StoredBits == 0)
            {
                return new ulong[0];
            }
            var temp = Clone();
            ulong[] result = new ulong[(int) IntDivideRoundUp(StoredBits, 64)];
            int i = 0;
            for (; temp.StoredBits > 64; i++)
            {
                result[i] = temp.Read(64);
            }
            int shift = 64 - (int) temp.StoredBits;
            result[i] = temp.Read((int) temp.StoredBits) << shift;
            return result;
        }

		/// <summary>
		/// Get the bits stored in a byte array (earlier added elements are at lower indexes)
		/// </summary>
		/// <returns>byte array of bits</returns>
		public byte[] GetByteArray()
		{
            if (StoredBits == 0)
            {
                return new byte[0];
            }
            var temp = Clone();
            byte[] result = new byte[(int) IntDivideRoundUp(StoredBits, 8)];
            int i = 0;
            for (; temp.StoredBits > 8; i++)
            {
                result[i] = (byte) temp.Read(8);
            }
            int shift = 8 - (int) temp.StoredBits;
            result[i] = (byte) (temp.Read((int) temp.StoredBits) << shift);
            return result;
        }

		/// <summary>
		/// Get the bits stored in a BitArray
		/// </summary>
		/// <returns>all bits in the stream in a BitArray</returns>
		public BitArray GetBitArray()
		{
			BitArray ba = new BitArray((int) StoredBits);
			var tempBuf = buffer.ToArray();
			int counter = 0;

            for (int i = 0; i < scratchReadBits; i++)
            {
                ba[counter] = (scratchReadBits & (1u << (63 - i))) > 0;
                counter++;
            }

            for (int i = 0; i < buffer.Count * 64; i++)
			{
				for (int j = 0; j < 64; j++)
				{
					ba[counter] = (tempBuf[i] & (1u << (63 - j))) > 0;
					counter++;
				}
			}

			for (int i = 0; i < scratchWriteBits; i++)
			{
				ba[counter] = (scratchWrite & (1u << (63 - i))) > 0;
				counter++;
			}

			return ba;
		}

        #region Write
        /// <summary>
        /// Write bytes of data to the stream.
        /// </summary>
        /// <param name="data">data to be written</param>
        /// <param name="offset">where in the byte array to start writing</param>
        /// <param name="length">how many bytes to write</param>
        public void Write(byte[] data, int offset, int length)
        {
            if (offset < 0)
            {
                throw new ArgumentException("offset cannot be less than zero");
            }
            else if (length + offset > data.Length)
            {
                throw new ArgumentException("(offset + length) cannot be greater than data.Length");
            }
            for (int i = offset; i < length; i++)
            {
                Write(data[i], 8);
            }
        }

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="bits">how many bits. Range(0, 64]</param>
		public void Write(ulong data, int bits)
		{
			if (bits <= 0 || bits > 64)
			{
				return;
			}
			scratchWrite |= ((data << (64 - bits)) >> scratchWriteBits);
			scratchWriteBits += bits;

			if (scratchWriteBits >= 64)
			{
				buffer.Enqueue(scratchWrite);
				scratchWrite = 0;
				scratchWriteBits -= 64;
				if (scratchWriteBits > 0)
				{
					scratchWrite |= (data << (64 - scratchWriteBits));
				}
			}
			StoredBits += bits;
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		public void Write(long data, long min, long max)
		{
			if (min > max)
			{
				min ^= max;
				max ^= min;
				min ^= max;
			}
			if (data < min || data > max)
			{
				throw new ArgumentOutOfRangeException("data", data, "must be between min and max");
			}

			ulong bdata = 0;
			ulong bmin = 0;
			ulong bmax = 0;

			if (max < 0)
			{
				max -= min;
				bmax = (ulong) max;
			}
			else
			{
				bmax = (ulong) max;
				if (min == long.MinValue)
				{
					bmax += (ulong) long.MaxValue + 1;
				}
				else if (min < 0)
				{
					bmin = (ulong) -min;
					bmax += bmin;
				}
				else
				{
					bmin = (ulong) min;
					bmax -= bmin;
				}
			}
			data -= min;
			bdata = (ulong) data;

			Write(bdata, BitsRequired(bmax));
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		public void Write(ulong data, ulong min, ulong max)
		{
			if (min > max)
			{
				min ^= max;
				max ^= min;
				min ^= max;
			}
			if (data < min || data > max)
			{
				throw new ArgumentOutOfRangeException("data", data, "must be between min and max");
			}

			if (min > 0)
			{
				max -= min;
			}

			Write(data, BitsRequired(max));
		}

		/// <summary>
		/// Write 1 bit to the stream
		/// </summary>
		/// <param name="data">bit to be written</param>
		public void Write(bool data)
		{
			Write(Convert.ToByte(data), 1);
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		public void Write(byte data, byte min, byte max)
		{
			Write((ulong) data, min, max);
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		public void Write(sbyte data, sbyte min, sbyte max)
		{
			Write((long) data, min, max);
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		public void Write(char data, char min, char max)
		{
			Write((ulong) data, min, max);
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		public void Write(short data, short min, short max)
		{
			Write((long) data, min, max);
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		public void Write(ushort data, ushort min, ushort max)
		{
			Write((ulong) data, min, max);
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		public void Write(int data, int min, int max)
		{
			Write((long) data, min, max);
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		public void Write(uint data, uint min, uint max)
		{
			Write((ulong) data, min, max);
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		/// <param name="precision">how many digits after the decimal</param>
		public void Write(float data, float min, float max, byte precision)
		{
			if (min > max)
			{
				var temp = min;
				min = max;
				max = temp;
			}
			if (data < min || data > max)
			{
				throw new ArgumentOutOfRangeException("data", data, "must be between min and max");
			}

			float newMax = max - min;
			int mult = IntPow(10, precision);

			if (float.IsInfinity(newMax))
			{
				Write((ulong) BitConverter.ToInt32(BitConverter.GetBytes(data), 0), 32);
				return;
			}
			else
			{
				double infoMax = Math.Round(newMax * mult, MidpointRounding.AwayFromZero);
				if (infoMax > uint.MaxValue)
				{
					Write((ulong)BitConverter.ToInt32(BitConverter.GetBytes(data), 0), 32);
					return;
				}

				data -= min;
				uint info = (uint) Math.Round(data * mult, MidpointRounding.AwayFromZero);
				Write(info, BitsRequired((ulong) infoMax));
			}
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <param name="data">bits to be written</param>
		/// <param name="min">the minimum number that can be written</param>
		/// <param name="max">the maximum number that can be written</param>
		/// <param name="precision">how many digits after the decimal</param>
		public void Write(double data, double min, double max, byte precision)
		{
			if (min > max)
			{
				var temp = min;
				min = max;
				max = temp;
			}
			if (data < min || data > max)
			{
				throw new ArgumentOutOfRangeException("data", data, "must be between min and max");
			}

			double newMax = max - min;
			int mult = IntPow(10, precision);

			if (double.IsInfinity(newMax))
			{
				Write((ulong) BitConverter.DoubleToInt64Bits(data), 64);
				return;
			}
			else
			{
				double infoMax = Math.Round(newMax * mult, MidpointRounding.AwayFromZero);
				if (infoMax > ulong.MaxValue)
				{
					Write((ulong) BitConverter.DoubleToInt64Bits(data), 64);
					return;
				}

				data -= min;
				ulong info = (ulong) Math.Round(data * mult, MidpointRounding.AwayFromZero);
				Write(info, BitsRequired((ulong) infoMax));
			}
		}

		/// <summary>
		/// Write bits to the stream
		/// </summary>
		/// <typeparam name="TEnum">The type of enum to be written</typeparam>
		/// <param name="data">bits to be written</param>
		/// <param name="min">minimum enum that can be written</param>
		/// <param name="max">maximum enum that can be written</param>
		public void Write<TEnum>(TEnum data, TEnum min, TEnum max) where TEnum : struct, IComparable, IFormattable, IConvertible
		{
			if (IsSigned(typeof(TEnum)))
			{
				var dataNum = checked(data.ToInt64(NumberFormatInfo.InvariantInfo));
				var minNum = checked(min.ToInt64(NumberFormatInfo.InvariantInfo));
				var maxNum = checked(max.ToInt64(NumberFormatInfo.InvariantInfo));
				Write(dataNum, minNum, maxNum);
			}
			else
			{
				var dataNum = checked(data.ToUInt64(NumberFormatInfo.InvariantInfo));
				var minNum = checked(min.ToUInt64(NumberFormatInfo.InvariantInfo));
				var maxNum = checked(max.ToUInt64(NumberFormatInfo.InvariantInfo));
				Write(dataNum, minNum, maxNum);
			}
		}
		#endregion

		#region Read

		/// <summary>
		/// Read bits from the stream. Returns 0 if bits > StoredBits.
		/// </summary>
		/// <param name="bits">How many bits to read. Range(0, 64]</param>
		/// <returns>bits read in ulong form</returns>
		public ulong Read(int bits)
		{
			if (bits <= 0 || bits > 64)
			{
				return 0;
			}
			StoredBits -= bits;

			ulong data = scratchRead >> (64 - bits);

			if (scratchReadBits < bits)
			{
				ulong tempScratch;
				int difference = bits - scratchReadBits;
				if (buffer.Count == 0)
				{
					scratchRead |= (scratchWrite >> scratchReadBits);
					data = scratchRead >> (64 - bits);
					scratchWrite <<= difference;
					scratchWriteBits -= difference;
					scratchRead = 0;
					scratchReadBits = 0;
				}
				else
				{
					tempScratch = buffer.Dequeue();
					scratchRead |= (tempScratch >> scratchReadBits);
					data = scratchRead >> (64 - bits);
					tempScratch <<= difference;
					scratchRead = tempScratch;
					scratchReadBits = 64 - difference;
				}
			}
			else
			{
				scratchRead <<= bits;
				scratchReadBits -= bits;
			}

			if (StoredBits < 0) // handle the case of asking for more bits than exist in the stream
			{
				data = 0;
				StoredBits = 0;
				scratchWrite = 0;
				scratchWriteBits = 0;
				scratchRead = 0;
				scratchReadBits = 0;
			}

			return data;
		}

		/// <summary>
		/// Read a bit from the stream and write it to data
		/// If data was read properly, returns true. Otherwise, returns false and sets data to false.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out bool data)
		{
			if (StoredBits == 0)
			{
				data = false;
				return false;
			}
			data = Read(1) > 0;
			return true;
		}

		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out ulong data, ulong min, ulong max)
		{
			if (min > max)
			{
				min ^= max;
				max ^= min;
				min ^= max;
			}

			max -= min;
            long oldBits = StoredBits;

			int bits = BitsRequired(max);
			data = Read(bits) - min;
			if (bits > oldBits || data > max)
			{
				data = 0;
				return false;
			}
			return true;
		}

		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out uint data, uint min, uint max)
		{
			ulong tempData;
			bool success = Read(out tempData, min, max);
			data = (uint) tempData;
			return success;
		}

		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out ushort data, ushort min, ushort max)
		{
			ulong tempData;
			bool success = Read(out tempData, min, max);
			data = (ushort) tempData;
			return success;
		}

		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out byte data, byte min, byte max)
		{
			ulong tempData;
			bool success = Read(out tempData, min, max);
			data = (byte) tempData;
			return success;
		}

		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to '\0'.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out char data, char min, char max)
		{
			ulong tempData;
			bool success = Read(out tempData, min, max);
			data = (char) tempData;
			return success;
		}

		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out long data, long min, long max)
		{
			if (min > max)
			{
				min ^= max;
				max ^= min;
				min ^= max;
			}

			ulong bmin = 0;
			ulong bmax = 0;

			if (max < 0)
			{
				max -= min;
				bmax = (ulong) max;
			}
			else
			{
				bmax = (ulong) max;
				if (min == long.MinValue)
				{
					bmax += (ulong) long.MaxValue + 1;
				}
				else if (min < 0)
				{
					bmin = (ulong) -min;
					bmax += bmin;
				}
				else
				{
					bmin = (ulong) min;
					bmax -= bmin;
				}
            }
            long oldBits = StoredBits;

            int bits = BitsRequired(bmax);
			data = (long) (Read(bits) - bmin);
			if (bits > oldBits || data > max)
			{
				data = 0;
				return false;
			}
			return true;
		}

		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out int data, int min, int max)
		{
			long tempData;
			bool success = Read(out tempData, min, max);
			data = (int) tempData;
			return success;
		}

		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out short data, short min, short max)
		{
			long tempData;
			bool success = Read(out tempData, min, max);
			data = (short) tempData;
			return success;
		}

		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out sbyte data, sbyte min, sbyte max)
		{
			long tempData;
			bool success = Read(out tempData, min, max);
			data = (sbyte) tempData;
			return success;
		}


		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		/// <param name="precision">how many digits after the decimal</param>
		public bool Read(out double data, double min, double max, byte precision)
		{
			if (min > max)
			{
				var temp = min;
				min = max;
				max = temp;
			}

			double newMax = max - min;
			int mult = IntPow(10, precision);

            long oldBits = StoredBits;

            if (double.IsInfinity(newMax))
			{
				data = BitConverter.Int64BitsToDouble((long) Read(64));
				if (oldBits < 64 || data < min || data > max)
				{
					data = 0;
					return false;
				}
			}
			else
			{
				double infoMax = Math.Round(newMax * mult, MidpointRounding.AwayFromZero);
				if (infoMax > ulong.MaxValue)
				{
					data = BitConverter.Int64BitsToDouble((long) Read(64));
					if (oldBits < 64 || data < min || data > max)
					{
						data = 0;
						return false;
					}
					return true;
				}

				int bits = BitsRequired((ulong) infoMax);
				data = (long) Read(bits) / (double) mult;
				if (bits > oldBits || data < min || data > max)
				{
					data = 0;
					return false;
				}
			}
			return true;
		}


		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <param name="data">the variable to be written to</param>
		/// <param name="min">the smallest possible number that could have been written</param>
		/// <param name="max">the largest possible number that could have been written</param>
		/// <param name="precision">how many digits after the decimal</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read(out float data, float min, float max, byte precision)
		{
			if (min > max)
			{
				var temp = min;
				min = max;
				max = temp;
			}

			float newMax = max - min;
            int mult = IntPow(10, precision);

            long oldBits = StoredBits;

			if (float.IsInfinity(newMax))
			{
                data = BitConverter.ToSingle(BitConverter.GetBytes((int) Read(32)), 0);
				if (oldBits < 32 || data < min || data > max)
				{
                    data = 0;
					return false;
				}
			}
			else
            {
                double infoMax = Math.Round(newMax * mult, MidpointRounding.AwayFromZero);
				if (infoMax > uint.MaxValue)
				{
					data = BitConverter.ToSingle(BitConverter.GetBytes((int) Read(32)), 0);
                    if (oldBits < 32 || data < min || data > max)
                    {
                        data = 0;
                        return false;
                    }
                    return true;
				}

				int bits = BitsRequired((uint) infoMax);
				data = (int) Read(bits) / (float) mult;
				if (bits > oldBits || data < min || data > max)
				{
                    data = 0;
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// Read bits from the stream and write that information to data.
		/// If data was read properly, returns true. Otherwise, returns false and sets data to 0.
		/// </summary>
		/// <typeparam name="TEnum">The type of enum to be written</typeparam>
		/// <param name="data">bits to be written</param>
		/// <param name="min">minimum enum that can be written</param>
		/// <param name="max">maximum enum that can be written</param>
		/// <returns>true if data was read properly, false otherwise</returns>
		public bool Read<TEnum>(out TEnum data, TEnum min, TEnum max) where TEnum : struct, IComparable, IFormattable, IConvertible
		{
			if (IsSigned(typeof(TEnum)))
			{
				long dataNum;
				var minNum = checked(min.ToInt64(NumberFormatInfo.InvariantInfo));
				var maxNum = checked(max.ToInt64(NumberFormatInfo.InvariantInfo));
				bool success = Read(out dataNum, minNum, maxNum);
				data = checked((TEnum) Enum.ToObject(typeof(TEnum), dataNum));
				return success;
			}
			else
			{
				ulong dataNum;
				var minNum = checked(min.ToUInt64(NumberFormatInfo.InvariantInfo));
				var maxNum = checked(max.ToUInt64(NumberFormatInfo.InvariantInfo));
				bool success = Read(out dataNum, minNum, maxNum);
				data = checked((TEnum) Enum.ToObject(typeof(TEnum), dataNum));
				return success;
			}
		}
		#endregion

		/// <summary>
		/// Take all the bits from other and add to this bit stream.
		/// NOTE: makes other empty
		/// </summary>
		/// <param name="other">bitstream to be written from</param>
		public void Merge(BitStream other)
		{
			if (ReferenceEquals(other, this))
			{
				throw new ArgumentException("Cannot merge a bit stream with itself. Other must be another instance of BitStream.", "other");
            }
            Write(other.scratchRead >> (64 - other.scratchReadBits), other.scratchReadBits);
            while (other.buffer.Count > 0)
            {
                Write(other.buffer.Dequeue(), 64);
            }
            Write(other.scratchWrite >> (64 - other.scratchWriteBits), other.scratchWriteBits);

            other.scratchRead = 0;
            other.scratchReadBits = 0;
            other.scratchWrite = 0;
            other.scratchWriteBits = 0;
            other.StoredBits = 0;
        }

        /// <summary>
        /// Take number of bits from start of other and add to end of this bit stream.
        /// </summary>
        /// <param name="other">bitstream to be written from</param>
        /// <param name="bits">number of bits to merge</param>
        public void Merge(BitStream other, long bits)
        {
            if (ReferenceEquals(other, this))
            {
                throw new ArgumentException("Cannot merge a bit stream with itself. Other must be another instance of BitStream.", "other");
            }
            else if (bits > other.StoredBits || bits <= 0)
            {
                throw new ArgumentException("Bits must be greater than 0 and less than other.StoredBits.", "bits");
            }
            while (bits > 64)
            {
                Write(other.Read(64), 64);
                bits -= 64;
            }
            Write(other.Read((int) bits), (int) bits);
        }

		/// <summary>
		/// Create a clone (deep copy) of this bit stream
		/// </summary>
		/// <returns>a clone (deep copy) of this bit stream</returns>
		public BitStream Clone()
		{
			Queue<ulong> newBuffer = new Queue<ulong>(buffer.Count);
			foreach (ulong data in buffer)
			{
				newBuffer.Enqueue(data);
			}
			return new BitStream()
			{
				buffer = newBuffer,
				scratchRead = scratchRead,
				scratchReadBits = scratchReadBits,
				scratchWrite = scratchWrite,
				scratchWriteBits = scratchWriteBits,
				StoredBits = StoredBits
			};
		}

		/// <summary>
		/// Clears this bit stream
		/// </summary>
		public void Clear()
		{
			buffer.Clear();
			scratchRead = 0;
			scratchReadBits = 0;
			scratchWrite = 0;
			scratchWriteBits = 0;
			StoredBits = 0;
		}

		/// <summary>
		/// Returns how many bits it takes to store a number
		/// </summary>
		/// <param name="max">the maximum number that will be written</param>
		/// <returns>how many bits are needed</returns>
		protected int BitsRequired(ulong max)
		{
			if (max == 0)
			{
				return 1;
			}
			for (int i = 1; i < 64; i++)
			{
				if (max < ((ulong) 1 << i))
				{
					return i;
				}
			}
			return 64;
		}

		/// <summary>
		/// If scratch_read contains any bits, moves them to the head of the buffer.
		/// NOTE: Not a short operation, use only when necessary!
		/// </summary>
		protected void ResetBuffer()
		{
			if (scratchReadBits > 0 && scratchRead != scratchWrite)
			{
				StoredBits = 0;
				var oldBuf = buffer.ToArray();
				buffer.Clear();
				var tempScratch = scratchWrite >> (64 - scratchWriteBits);
				int tempBits = scratchWriteBits;
				scratchWrite = 0;
				scratchWriteBits = 0;
				Write(scratchRead >> (64 - scratchReadBits), scratchReadBits);
				scratchRead = 0;
				scratchReadBits = 0;
				for (int i = 0; i < oldBuf.Length; i++)
				{
					Write(oldBuf[i], 64);
				}
				Write(tempScratch, tempBits);
			}
		}

		long IntDivideRoundUp(long upper, long lower)
		{
			return (upper + lower - 1) / lower;
		}

		int IntPow(int x, uint pow)
		{
			int ret = 1;
			while (pow != 0)
			{
				if ((pow & 1) == 1)
				{
					ret *= x;
				}
				x *= x;
				pow >>= 1;
			}
			return ret;
		}

		bool IsSigned(Type tEnum)
		{
			var underlyingType = Enum.GetUnderlyingType(tEnum);
			if (underlyingType == typeof(long) || underlyingType == typeof(int) || underlyingType == typeof(short) || underlyingType == typeof(sbyte))
				return true;
			if (underlyingType == typeof(ulong) || underlyingType == typeof(uint) || underlyingType == typeof(ushort) || underlyingType == typeof(byte))
				return false;
			throw new ArgumentException(tEnum.Name + " is not an enum type");
		}
	}
}