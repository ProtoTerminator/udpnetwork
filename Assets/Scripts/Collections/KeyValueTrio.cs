namespace Arq.Collections
{
    public struct KeyValueTrio<TKey1, TKey2, TValue>
    {
        public KeyValueTrio(TKey1 key1, TKey2 key2, TValue value)
        {
            Key1 = key1;
            Key2 = key2;
            Value = value;
        }

        public TKey1 Key1
        {
            get;
            private set;
        }
        public TKey2 Key2
        {
            get;
            private set;
        }
        public TValue Value
        {
            get;
            private set;
        }

        public override string ToString()
        {
            return "Key1: " + Key1 + ", Key2: " + Key2 + ", Value: " + Value;
        }
    }
}