using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Arq.Networking;
using Arq.Collections;

public class NetworkDataTest : NetworkBehaviour
{
    public float serverCounter = 0;
    public ulong localCounter = 0;

    private void Update()
    {
        if (IsLocalPlayer)
        {
            localCounter++;
        }
#if SERVER
        serverCounter += Time.deltaTime;
#endif
    }

    public override bool Serialize(BitSerializer bitSerializer)
    {
        if (bitSerializer.IsReading)
        {
#if !SERVER
            bitSerializer.Serialize(ref serverCounter, float.MinValue, float.MaxValue, 0);
#endif
            if (IsLocalPlayer)
            {
            }
            else
            {
                bitSerializer.Serialize(ref localCounter, ulong.MinValue, ulong.MaxValue);
            }
        }
        else
        {
#if SERVER
            bitSerializer.Serialize(ref serverCounter, float.MinValue, float.MaxValue, 0);
#endif
            if (IsLocalPlayer)
            {
                bitSerializer.Serialize(ref localCounter, ulong.MinValue, ulong.MaxValue);
            }
            else
            {
            }
        }
        return true;
    }
}